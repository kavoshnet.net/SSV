﻿using System;
using System.Data;
using System.Drawing;
using System.Net.Mime;
using System.Web.UI;
using System.Web.UI.WebControls;
using BijanComponents;
using dstpersonTableAdapters;
using dsvpersonTableAdapters;
using FastReport;
using FastReport.Data;

public partial class Sabttozihat : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Label6.Text = "";
        if ((Session["VOROD"] != null) && (Session["ISADMIN"] != null))
        {
            if ((Session["VOROD"].ToString() != "ok") || (Session["ISADMIN"].ToString() != "1"))
            {
                Response.Redirect("Default.aspx");
            }
        }
        else
        {
            Response.Redirect("login.aspx");
        }

    }

    protected void btnprint_Click(object sender, EventArgs e)
    {
        string tozihat = TextBox1.Text;
        Session["tozihat"] = TextBox1.Text;
        
        Response.Redirect("printtozihat.aspx");
    }
}