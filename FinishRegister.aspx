﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
         CodeFile="FinishRegister.aspx.cs" Inherits="FinishRegister" Title="تکمیل ثبت نام" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"><p class="panel-info" style="color: darkgreen" >  اطلاعات شما با موفقیت ثبت گردید . شما از اکنون می توانید وارد سایت گردیده و اطلاعات
                خود را بدست آورید</p>
            </div>
            <div class="panel-body">
                <img src="Images/Video.png" />
                <a href="Default.aspx">بازگشت به صفحه اصلی</a>
            </div>
            </div>
            </div>
<div class="col-lg-12">
</div>                
            </div>
            </div>

</asp:Content>