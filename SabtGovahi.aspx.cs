﻿using System;
using System.Data;
using System.Drawing;
using System.Web.Services.Description;
using System.Web.UI;
using System.Web.UI.WebControls;
using BijanComponents;
using DevExpress.Web.ASPxGridView;
using dstpersonTableAdapters;
using dsvpersonTableAdapters;

public partial class SabtGovahi : Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        lblError.Text = "";
        if ((Session["VOROD"] != null) && (Session["ISADMIN"] != null))
        {
            if ((Session["VOROD"].ToString() == "ok") && (Session["ISADMIN"].ToString() == "2"))
            {
                if (!IsPostBack)
                {
                    jqdbdate.Text = ShamsiDate.GetShamsiDate(DateTime.Now).Trim();
                    jqdrdatef.Text = ShamsiDate.GetShamsiDate(DateTime.Now.AddDays(-1)).Trim();
                    jqdrdatet.Text = ShamsiDate.GetShamsiDate(DateTime.Now).Trim();
                    BindGrid();
                    getdata();
                    //jqdbdate.MaxDate = DateTime.Now;
                }
            }
            else
            {
                lblError.Text = "شما اجازه استفاده از این بخش را ندارید";
                Response.Redirect("Default.aspx");

            }
        }
        else
        {

            Response.Redirect("login.aspx");
        }
        MasterASPxdgperson.DataColumns["mother"].Settings.AutoFilterCondition = AutoFilterCondition.Contains;
        MasterASPxdgperson.DataColumns["addr"].Settings.AutoFilterCondition = AutoFilterCondition.Contains;
        MasterASPxdgperson.DataColumns["father"].Settings.AutoFilterCondition = AutoFilterCondition.Contains;

    }

    private void BindGrid()
    {
        try
        {

            //var sda = new vpersonTableAdapter();
            //var ds = new dsvperson();
            int iduser = Convert.ToInt16(Session["IDUSER"]);
            string datesabtfrom = jqdrdatef.Text;//ShamsiDate.GetShamsiDate(DateTime.Now.AddDays(-365)).Trim();
            string datesabtto = jqdrdatet.Text;//ShamsiDate.GetShamsiDate(DateTime.Now).Trim();
            int codemahaluser = Convert.ToInt16(Session["CODEMAHAL"]);
            /*sda.FillByoutbox(ds.vperson, iduser, datesabtfrom, datesabtto);
            dgperson.DataSource = ds.vperson;
            dgperson.DataBind();*/

            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            Mastersqldperson.SelectParameters.Clear();
            Mastersqldperson.SelectParameters.Add("datesabtfrom", datesabtfrom);
            Mastersqldperson.SelectParameters.Add("datesabtto", datesabtto);
            Mastersqldperson.SelectParameters.Add("iduser", iduser.ToString());
            Mastersqldperson.DataBind();
            MasterASPxdgperson.DataBind();
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------


            /*DetailASPxdgperson.DataSource = ds.vperson;
             DetailASPxdgperson.DataBind();*/

        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
            lblError.Text = "خطای ناشناخته رخ داده است.";
        }
    }

    private void getdata()
    {
        /*Session["IDUSER"] Session["IDMAHAL"] Session["FNAME"] Session["LNAME"] 
        Session["SEX"] Session["SEMAT"] Session["MKHEDMAT"] Session["ADDR"] 
        Session["NAMEMAHAL"] Session["CODEMAHAL"] */
        txtfname.Text = Session["FNAME"].ToString();
        txtlname.Text = Session["LNAME"].ToString();
        rblsemat.SelectedValue = (Session["SEMAT"].ToString());
        txtmkhedmat.Text = Session["NAMEMAHAL"].ToString();
        txtaddr.Text = Session["ADDR"].ToString();
        txtbimarestan.Text = Session["MKHEDMAT"].ToString();
        ddlmtavalod.SelectedValue = Session["IDMAHAL"].ToString();
        ddlmsokonat.SelectedValue = Session["IDMAHAL"].ToString();
        txtcityvilage.Text = Session["NAMEMAHAL"].ToString();
        txtcityvilagesokonat.Text = Session["NAMEMAHAL"].ToString();
        txtostan.Text = Session["NAMEMAHAL"].ToString();
        txtostansokonat.Text = Session["NAMEMAHAL"].ToString();

    }

    protected void btnInsert_Click(object sender, EventArgs e)
    {
        try
        {
            var sda = new tpersonTableAdapter();
            var ds = new dstperson();
            DateTime mybdate, bf, bt;

            //چک تاریخ تولد
            mybdate = ShamsiDate.ShamsiToGregory(jqdbdate.Text.Trim());
            bf = DateTime.Now.AddDays(-365);
            bt = DateTime.Now;
            if (mybdate > bt || mybdate < bf)
            {
                lblError.Text = "در ثبت تاریخ تولد دقت کنید";
                return;
            }
            bf = DateTime.Now.AddDays(-15);
            bt = DateTime.Now;
            if (mybdate > bt || mybdate < bf)
            {
                lblBDError.Visible = true;
                lblBDError.ForeColor = Color.Green;
                lblBDError.Text = "ثبت واقعه در خارج از مهلت قانونی صورت میگیرد";
                //return;
            }





            if (MasterASPxdgperson.FocusedRowIndex != -1 /*  && txtLname.Text.Trim()!=""*/)
            {
                bool Success = sda.FillByidperson(ds.tperson, int.Parse(MasterASPxdgperson.GetRowValues(MasterASPxdgperson.FocusedRowIndex, "idperson").ToString())) > 0;
                if (Success)
                {
                    //نوزادds.tperson.Rows[0][ds.tperson.nameColumn] = txtname.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.sexColumn] = rblsex.SelectedValue;
                    ds.tperson.Rows[0][ds.tperson.bdateColumn] = jqdbdate.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.houreColumn] = txthoure.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.minuteColumn] = txtminute.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.bimarestanColumn] = txtbimarestan.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.khanebehdashtColumn] = txtkhanebehdasht.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.nmtavalodColumn] = rblnmtavaload.SelectedItem;
                    ds.tperson.Rows[0][ds.tperson.ostanColumn] = txtostan.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.shahrestanColumn] = ddlmtavalod.SelectedValue;
                    ds.tperson.Rows[0][ds.tperson.bakhshColumn] = txtbakhsh.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.cityvilageColumn] = txtcityvilage.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.idmtavalodColumn] = ddlmtavalod.SelectedValue;
                    //پدر
                    ds.tperson.Rows[0][ds.tperson.fathernameColumn] = txtfathername.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.fatherfnameColumn] = txtfatherfname.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.ncfatherColumn] = txtncfather.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.bdfatherColumn] = jqdbdfather.Text.Trim();
                    //مادر
                    ds.tperson.Rows[0][ds.tperson.mothernameColumn] = txtmothername.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.motherfnameColumn] = txtmotherfname.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.ncmotherColumn] = txtncmother.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.bdmotherColumn] = jqdbdmother.Text.Trim();
                    //محل سکونت
                    //ds.tperson.Rows[0][ds.tperson.ostanColumn] = txtostan.Text.Trim();
                    //ds.tperson.Rows[0][ds.tperson.shahrestanColumn] = txtshahrestan.Text.Trim();
                    //ds.tperson.Rows[0][ds.tperson.bakhshColumn] = txtbakhsh.Text.Trim();
                    //ds.tperson.Rows[0][ds.tperson.cityvilageColumn] = txtcityvilage.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.idmsokonatColumn] = ddlmsokonat.SelectedValue;
                    ds.tperson.Rows[0][ds.tperson.codepostiColumn] = txtcodeposti.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.tellColumn] = txttell.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.mob1Column] = txtmob1.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.addrColumn] = txtaddrsokonat.Text.Trim();


                    /*ds.tperson.Rows[0][ds.tperson.addrColumn] = txtaddr1.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.idmsokonatColumn] = ddlmsokonat11.SelectedValue;
                    ds.tperson.Rows[0][ds.tperson.tellColumn] = txttell1.Text.Trim().PadLeft(11,'0');
                    ds.tperson.Rows[0][ds.tperson.mob1Column] = txtmob1.Text.Trim().PadLeft(11, '0');
                    ds.tperson.Rows[0][ds.tperson.mob2Column] = txtmob2.Text.Trim().PadLeft(11, '0');*/
                    ds.tperson.Rows[0][ds.tperson.dsabtColumn] = ShamsiDate.GetShamsiDate(DateTime.Now).Trim();
                    ds.tperson.Rows[0][ds.tperson.demalColumn] = "";
                    ds.tperson.Rows[0][ds.tperson.emalColumn] = false;
                    ds.tperson.Rows[0][ds.tperson.idusersabtColumn] = Convert.ToInt16(Session["IDUSER"]);

                    ds.tperson.Rows[0][ds.tperson.shomareColumn] = txtshomare.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.seriColumn] = txtseri.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.serialColumn] = txtserial.Text.Trim();

                    sda.Update(ds.tperson);
                    lblError.Text = "ویرایش داده ها با موفقیت در انجام شد.";
                    Session["GOVAHIID"] = ds.tperson.Rows[0][ds.tperson.idpersonColumn];
                }
            }
            else
            {
                DataRow MyRow = ds.tperson.NewRow();

                //نوزاد
                MyRow[ds.tperson.nameColumn] = txtname.Text.Trim();
                MyRow[ds.tperson.sexColumn] = rblsex.SelectedValue;
                MyRow[ds.tperson.bdateColumn] = jqdbdate.Text.Trim();
                MyRow[ds.tperson.houreColumn] = txthoure.Text.Trim();
                MyRow[ds.tperson.minuteColumn] = txtminute.Text.Trim();
                MyRow[ds.tperson.bimarestanColumn] = txtbimarestan.Text.Trim();
                MyRow[ds.tperson.khanebehdashtColumn] = txtkhanebehdasht.Text.Trim();
                MyRow[ds.tperson.nmtavalodColumn] = rblnmtavaload.SelectedItem;
                MyRow[ds.tperson.ostanColumn] = txtostan.Text.Trim();
                MyRow[ds.tperson.shahrestanColumn] = ddlmtavalod.Text.Trim();
                MyRow[ds.tperson.bakhshColumn] = txtbakhsh.Text.Trim();
                MyRow[ds.tperson.cityvilageColumn] = txtcityvilage.Text.Trim();
                MyRow[ds.tperson.idmtavalodColumn] = ddlmtavalod.SelectedValue;

                //پدر
                MyRow[ds.tperson.fathernameColumn] = txtfathername.Text.Trim();
                MyRow[ds.tperson.fatherfnameColumn] = txtfatherfname.Text.Trim();
                MyRow[ds.tperson.ncfatherColumn] = txtncfather.Text.Trim();
                MyRow[ds.tperson.bdfatherColumn] = jqdbdfather.Text.Trim();

                //مادر
                MyRow[ds.tperson.mothernameColumn] = txtmothername.Text.Trim();
                MyRow[ds.tperson.motherfnameColumn] = txtmotherfname.Text.Trim();
                MyRow[ds.tperson.ncmotherColumn] = txtncmother.Text.Trim();
                MyRow[ds.tperson.bdmotherColumn] = jqdbdmother.Text.Trim();

                //محل سکونت
                //MyRow[ds.tperson.ostanColumn] = txtostan.Text.Trim();
                //MyRow[ds.tperson.shahrestanColumn] = txtshahrestan.Text.Trim();
                //MyRow[ds.tperson.bakhshColumn] = txtbakhsh.Text.Trim();
                //MyRow[ds.tperson.cityvilageColumn] = txtcityvilage.Text.Trim();

                MyRow[ds.tperson.idmsokonatColumn] = ddlmsokonat.SelectedValue;
                MyRow[ds.tperson.codepostiColumn] = txtcodeposti.Text.Trim();
                MyRow[ds.tperson.tellColumn] = txttell.Text.Trim();
                MyRow[ds.tperson.mob1Column] = txtmob1.Text.Trim();
                MyRow[ds.tperson.addrColumn] = txtaddrsokonat.Text.Trim();


                /*MyRow[ds.tperson.addrColumn] = txtaddr1.Text.Trim();
                MyRow[ds.tperson.idmsokonatColumn] = ddlmsokonat11.SelectedValue;
                MyRow[ds.tperson.tellColumn] = txttell1.Text.Trim().PadLeft(11,'0');
                MyRow[ds.tperson.mob1Column] = txtmob1.Text.Trim().PadLeft(11, '0');
                MyRow[ds.tperson.mob2Column] = txtmob2.Text.Trim().PadLeft(11, '0');*/
                MyRow[ds.tperson.dsabtColumn] = ShamsiDate.GetShamsiDate(DateTime.Now).Trim();
                MyRow[ds.tperson.demalColumn] = "";
                MyRow[ds.tperson.emalColumn] = false;
                MyRow[ds.tperson.idusersabtColumn] = Convert.ToInt16(Session["IDUSER"]);

                MyRow[ds.tperson.shomareColumn] = txtshomare.Text.Trim();
                MyRow[ds.tperson.seriColumn] = txtseri.Text.Trim();
                MyRow[ds.tperson.serialColumn] = txtserial.Text.Trim();


                ds.tperson.Rows.Add(MyRow);
                sda.Update(ds.tperson);

                lblError.Text = "اطلاعات  جدید با موفقیت در سیستم ذخیره گردید";
                Session["GOVAHIID"] = ds.tperson.Rows[0][ds.tperson.idpersonColumn];
            }
            ClearTextBox();
            BindGrid();
            MasterASPxdgperson.FocusedRowIndex = 0;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
            lblError.Text = "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید.";
            Session["GOVAHIID"] = -1;
        }
    }

    private void ClearTextBox()
    {
        //نوزاد
        txtname.Text = "";
        rblsex.SelectedIndex = -1;
        jqdbdate.Text = ShamsiDate.GetShamsiDate(DateTime.Now).Trim();
        txthoure.Text = "";
        txtminute.Text = "";
        //txtbimarestan.Text = "";
        txtkhanebehdasht.Text = "";
        rblnmtavaload.SelectedIndex = -1;

        //پدر
        txtfathername.Text = "";
        txtfatherfname.Text = "";
        txtncfather.Text = "";
        jqdbdfather.Text = "";

        //مادر
        txtmothername.Text = "";
        txtmotherfname.Text = "";
        txtncmother.Text = "";
        jqdbdmother.Text = "";

        //محل سکونت
        txtaddrsokonat.Text = "";
        txtcodeposti.Text = "";
        txttell.Text = "";
        txtmob1.Text = "";
        txtshomare.Text = "";
        txtseri.Text = "";
        txtserial.Text = "";

        //        ddlmtavalod.SelectedIndex = -1;
        ddlmsokonat.SelectedIndex = -1;
        ddlmtavalod.SelectedValue = Session["IDMAHAL"].ToString();
        MasterASPxdgperson.FocusedRowIndex = -1;
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            if (MasterASPxdgperson.FocusedRowIndex != -1)
            //if (txtmother.Text.Trim() != "" && txtmother.Text.Trim()!="" && dgperson.SelectedIndex > -1)
            {
                var sda = new tpersonTableAdapter();
                sda.Delete(int.Parse(MasterASPxdgperson.GetRowValues(MasterASPxdgperson.FocusedRowIndex, "idperson").ToString()));
                lblError.Text = "حذف  با موفقیت انجام شد";
                BindGrid();
                ClearTextBox();
                MasterASPxdgperson.FocusedRowIndex = 0;
            }
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
            lblError.Text = "در حذف داده ها خطایی رخ داده است لطفا دوباره تلاش کنید.";
        }
    }

    protected void Cancel_Click(object sender, EventArgs e)
    {
        MasterASPxdgperson.FocusedRowIndex = -1;
        ClearTextBox();
        txtshomare.Focus();
    }

    protected void btnprint_Click(object sender, EventArgs e)
    {
        //if (dgperson.SelectedIndex != -1)
        if (Convert.ToInt16(Session["GOVAHIID"]) != -1)
            Response.Redirect("printgovahi.aspx");
    }
    protected void btnprn_Click(object sender, EventArgs e)
    {
        Session["fromdate"] = jqdrdatef.Text;
        Session["todate"] = jqdrdatet.Text;
        Response.Redirect("printamar1.aspx");
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton lnkbtn = (LinkButton)sender;
        GridViewRow gvrow = (GridViewRow)lnkbtn.NamingContainer;
        int id = gvrow.RowIndex;
        int value = Convert.ToInt32(lnkbtn.CommandArgument);
        var sda = new tpersonTableAdapter();
        var ds = new dstperson();
        if (id > -1)
        {
            bool Success = sda.FillByidperson(ds.tperson, value) > 0;
            Session["GOVAHIID"] = ds.tperson.Rows[0][ds.tperson.idpersonColumn];
            Response.Redirect("printgovahi.aspx");
        }


    }
    protected void btnrep_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void MasterASPxdgperson_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
        {
            string temp = e.GetValue("bdate").ToString();
            DateTime tow = ShamsiDate.ShamsiToGregory(temp);
            DateTime from = DateTime.Now;
            int y = 15 - from.Subtract(tow).Days;
            if (y >= 1 && y <= 5)
            {
                e.Row.BackColor = Color.Khaki;
            }
            else if (y < 1)
            {
                e.Row.BackColor = Color.Tomato;
            }
            Label label = MasterASPxdgperson.FindRowCellTemplateControl(e.VisibleIndex, null, "Label1") as Label;

            //var x = (Label)e.Row.Cells[7].FindControl("Label3");
            if (y > 0)
                label.Text = y + " روز ";
            else
                label.Text = "خارج";
        }

    }
    protected void DetailASPxdgperson_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["idperson"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    protected void LinkButton2_Click(object sender, EventArgs e)
    {

        try
        {
            var sda = new vpersonTableAdapter();
            var ds = new dsvperson();
            int selectedvalue = int.Parse(MasterASPxdgperson.GetRowValues(MasterASPxdgperson.FocusedRowIndex, "idperson").ToString());
            if (selectedvalue != 0)
            {
                bool Success = sda.FillByidperson(ds.vperson, selectedvalue) > 0;
                if (Success)
                {
                    /*sda.UpdateSetEmal(true, ShamsiDate.GetShamsiDate(DateTime.Now).Trim(),
                        Convert.ToInt16(Session["IDUSER"]), selectedvalue);*/
                    Session["GOVAHIID"] = int.Parse(MasterASPxdgperson.GetRowValues(MasterASPxdgperson.FocusedRowIndex, "idperson").ToString());
                    Response.Redirect("printgovahi.aspx");

                }
            }
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
            lblError.Text = "در اصلاح داده ها خطایی رخ داده است لطفا دوباره تلاش کنید.";
        }
    }
    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        var sda = new tpersonTableAdapter();
        var ds = new dstperson();
        sda.FillByidperson(ds.tperson, int.Parse(MasterASPxdgperson.GetRowValues(MasterASPxdgperson.FocusedRowIndex, "idperson").ToString()));
        Session["GOVAHIID"] =
            int.Parse(MasterASPxdgperson.GetRowValues(MasterASPxdgperson.FocusedRowIndex, "idperson").ToString());
        //نوزاد
        txtname.Text = ds.tperson.Rows[0][ds.tperson.nameColumn].ToString();
        rblsex.SelectedValue = ds.tperson.Rows[0][ds.tperson.sexColumn].ToString();
        jqdbdate.Text = ds.tperson.Rows[0][ds.tperson.bdateColumn].ToString();
        txthoure.Text = ds.tperson.Rows[0][ds.tperson.houreColumn].ToString();
        txtminute.Text = ds.tperson.Rows[0][ds.tperson.minuteColumn].ToString();
        txtbimarestan.Text = ds.tperson.Rows[0][ds.tperson.bimarestanColumn].ToString();
        txtkhanebehdasht.Text = ds.tperson.Rows[0][ds.tperson.khanebehdashtColumn].ToString();
        rblnmtavaload.SelectedValue = ds.tperson.Rows[0][ds.tperson.nmtavalodColumn].ToString();
        txtostan.Text = ds.tperson.Rows[0][ds.tperson.ostanColumn].ToString();
        ddlmtavalod.SelectedValue = ds.tperson.Rows[0][ds.tperson.shahrestanColumn].ToString();
        txtbakhsh.Text = ds.tperson.Rows[0][ds.tperson.bakhshColumn].ToString();
        txtcityvilage.Text = ds.tperson.Rows[0][ds.tperson.cityvilageColumn].ToString();
        ddlmtavalod.SelectedValue = ds.tperson.Rows[0][ds.tperson.idmtavalodColumn].ToString();
        //پدر
        txtfathername.Text = ds.tperson.Rows[0][ds.tperson.fathernameColumn].ToString();
        txtfatherfname.Text = ds.tperson.Rows[0][ds.tperson.fatherfnameColumn].ToString();
        txtncfather.Text = ds.tperson.Rows[0][ds.tperson.ncfatherColumn].ToString();
        jqdbdfather.Text = ds.tperson.Rows[0][ds.tperson.bdfatherColumn].ToString();
        //مادر
        txtmothername.Text = ds.tperson.Rows[0][ds.tperson.mothernameColumn].ToString();
        txtmotherfname.Text = ds.tperson.Rows[0][ds.tperson.motherfnameColumn].ToString();
        txtncmother.Text = ds.tperson.Rows[0][ds.tperson.ncmotherColumn].ToString();
        jqdbdmother.Text = ds.tperson.Rows[0][ds.tperson.bdmotherColumn].ToString();
        //محل سکونت
        //ds.tperson.Rows[0][ds.tperson.ostanColumn] = txtostan.Text.Trim();
        //ds.tperson.Rows[0][ds.tperson.shahrestanColumn] = txtshahrestan.Text.Trim();
        //ds.tperson.Rows[0][ds.tperson.bakhshColumn] = txtbakhsh.Text.Trim();
        //ds.tperson.Rows[0][ds.tperson.cityvilageColumn] = txtcityvilage.Text.Trim();
        ddlmsokonat.SelectedValue = ds.tperson.Rows[0][ds.tperson.idmsokonatColumn].ToString();
        txtcodeposti.Text = ds.tperson.Rows[0][ds.tperson.codepostiColumn].ToString();
        txttell.Text = ds.tperson.Rows[0][ds.tperson.tellColumn].ToString();
        txtmob1.Text = ds.tperson.Rows[0][ds.tperson.mob1Column].ToString();
        txtaddrsokonat.Text = ds.tperson.Rows[0][ds.tperson.addrColumn].ToString();

        txtshomare.Text = ds.tperson.Rows[0][ds.tperson.shomareColumn].ToString();
        txtseri.Text = ds.tperson.Rows[0][ds.tperson.seriColumn].ToString();
        txtserial.Text = ds.tperson.Rows[0][ds.tperson.serialColumn].ToString();


        /*ds.tperson.Rows[0][ds.tperson.addrColumn] = txtaddr1.Text.Trim();
        ds.tperson.Rows[0][ds.tperson.idmsokonatColumn] = ddlmsokonat11.SelectedValue;
        ds.tperson.Rows[0][ds.tperson.tellColumn] = txttell1.Text.Trim().PadLeft(11,'0');
        ds.tperson.Rows[0][ds.tperson.mob1Column] = txtmob1.Text.Trim().PadLeft(11, '0');
        ds.tperson.Rows[0][ds.tperson.mob2Column] = txtmob2.Text.Trim().PadLeft(11, '0');*/
        //ds.tperson.Rows[0][ds.tperson.dsabtColumn] = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now).Trim();
        //ds.tperson.Rows[0][ds.tperson.demalColumn] = "";
        //ds.tperson.Rows[0][ds.tperson.emalColumn] = false;
        ds.tperson.Rows[0][ds.tperson.idusersabtColumn] = Convert.ToInt16(Session["IDUSER"]);



        txtfathername.Text = ds.tperson.Rows[0][ds.tperson.fathernameColumn].ToString();
        txtfatherfname.Text = ds.tperson.Rows[0][ds.tperson.fatherfnameColumn].ToString();
        txtmothername.Text = ds.tperson.Rows[0][ds.tperson.mothernameColumn].ToString();
        txtmotherfname.Text = ds.tperson.Rows[0][ds.tperson.motherfnameColumn].ToString();
        //JQDatePicker1.Text = ds.tperson.Rows[0][ds.tperson.bdateColumn].ToString();
        ddlmtavalod.SelectedValue = ds.tperson.Rows[0][ds.tperson.idmtavalodColumn].ToString();
        if (ds.tperson.Rows[0][ds.tperson.nmtavalodColumn].ToString() == "منزل")
        {
            rblnmtavaload.SelectedIndex = 0;
        }
        else
        {
            rblnmtavaload.SelectedIndex = 1;
        }

        txtaddrsokonat.Text = ds.tperson.Rows[0][ds.tperson.addrColumn].ToString();
        ddlmsokonat.SelectedValue = ds.tperson.Rows[0][ds.tperson.idmsokonatColumn].ToString();
        txttell.Text = ds.tperson.Rows[0][ds.tperson.tellColumn].ToString();
        txtmob1.Text = ds.tperson.Rows[0][ds.tperson.mob1Column].ToString();

        //sda.Update(ds.tperson);
        lblError.Text = "در حال ویرایش داده ها.";
    }
    protected void btnnew_Click(object sender, EventArgs e)
    {
        MasterASPxdgperson.FocusedRowIndex = -1;
        ClearTextBox();
        txtshomare.Focus();
    }
}