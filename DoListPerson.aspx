﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
         CodeFile="DoListPerson.aspx.cs" Inherits="DoListPerson" Title="لیست ولادتهای انجام شده" %>

<%@ Register assembly="JQControls" namespace="JQControls" tagprefix="cc1" %>

<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"><p class="panel-info" style="color: darkgreen" >لیست متولدین ثبت شده در اداره ثبت احوال  <%= this.Session["NAMEMAHAL"].ToString()%></p>
            </div>
            <div class="panel-body">
    
                <dx:ASPxGridView ID="MasterASPxdgperson" runat="server" AutoGenerateColumns="False" 
                    EnableTheming="True" KeyFieldName="idperson" RightToLeft="True" Theme="Glass" 
                    Width="100%"  onhtmlrowprepared="MasterASPxdgperson_HtmlRowPrepared" 
                    DataSourceID="Mastersqldperson">

                    <Columns>
                        <dx:GridViewDataTextColumn Caption="مادر" FieldName="mother" VisibleIndex="0">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="پدر" FieldName="father" VisibleIndex="1" 
                            Visible="True">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="ش.م مادر" FieldName="ncmother" VisibleIndex="0">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="ش.م پدر" FieldName="ncfather" VisibleIndex="0">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="تاریخ تولد" FieldName="bdate" VisibleIndex="2" 
                            Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="تلفن" FieldName="tell" VisibleIndex="3" 
                            Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="موبایل" FieldName="mob1" VisibleIndex="5" 
                            Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="idperson" VisibleIndex="6" 
                            Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn VisibleIndex="9" Caption="کاربر ثبت" 
                            FieldName="nameusersabt">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="dsabt" 
                            VisibleIndex="10" Caption="تاریخ ثبت">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="مهلت" 
                            VisibleIndex="13">
                            <DataItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="شماره ملی پدر" FieldName="ncfather" 
                            VisibleIndex="4" Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="محل تولد" VisibleIndex="7" 
                            FieldName="namemtavalod" Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="محل سکونت" FieldName="namemsokonat" 
                            VisibleIndex="8" Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn Caption="جنسیت" FieldName="sex" VisibleIndex="5" 
                            Visible="False">
                            <PropertiesCheckEdit DisplayFormatString="پسر;دختر" DisplayTextChecked="پسر" 
                                DisplayTextUnchecked="دختر">
                            </PropertiesCheckEdit>
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn Caption="کاربراعمال" FieldName="nameuseremal" 
                            VisibleIndex="11">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="تاریخ اعمال" FieldName="demal" 
                            VisibleIndex="12">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <SettingsBehavior AllowFocusedRow="True" EnableRowHotTrack="True" />

                    <SettingsPager>
                        <Summary AllPagesText="صفحات : {0} - {1} ({2} مورد )" 
                            Text="صفحه {0} از {1} ({2} مورد)" />
                    </SettingsPager>

                    <Settings ShowFilterRow="True" />
                    <SettingsText EmptyDataRow="داده ای برای نمایش وجود ندارد" />
                    <SettingsDetail ShowDetailRow="True" />

                    <Styles>
                        <AlternatingRow Enabled="True">
                        </AlternatingRow>
                    </Styles>
                    <Templates>
                        <DetailRow>
                            <dx:ASPxGridView ID="DetailASPxdgperson" runat="server" 
                                AutoGenerateColumns="False" EnableTheming="True" KeyFieldName="idperson" RightToLeft="True" 
                                Theme="Office2003Blue" Width="100%" DataSourceID="Detailsqldperson" 
                                onbeforeperformdataselect="DetailASPxdgperson_BeforePerformDataSelect">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="مادر" FieldName="mother" VisibleIndex="0" 
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="پدر" FieldName="father" VisibleIndex="1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="تاریخ تولد" FieldName="bdate" 
                                        VisibleIndex="8">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="تلفن" FieldName="tell" VisibleIndex="2" 
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="موبایل" VisibleIndex="3" FieldName="mob1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="idperson" Visible="False" 
                                        VisibleIndex="5">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="کاربر ثبت" FieldName="nameusersabt" VisibleIndex="10" 
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="تاریخ ثبت" FieldName="dsabt" VisibleIndex="9" 
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="مهلت" 
                                        VisibleIndex="13" Visible="False">
                                        <DataItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Text="Label"></asp:Label>
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="شماره ملی پدر" VisibleIndex="4" 
                                        FieldName="ncfather">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="محل تولد" FieldName="namemtavalod" 
                                        VisibleIndex="6">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="محل سکونت" FieldName="namemsokonat" 
                                        VisibleIndex="7">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataCheckColumn Caption="جنسیت" FieldName="sex" VisibleIndex="5">
                                        <PropertiesCheckEdit DisplayFormatString="پسر;دختر" DisplayTextChecked="پسر" 
                                            DisplayTextUnchecked="دختر">
                                        </PropertiesCheckEdit>
                                    </dx:GridViewDataCheckColumn>
                                    <dx:GridViewDataTextColumn Caption="کاربراعمال" FieldName="nameuseremal" 
                                        VisibleIndex="12" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="تاریخ اعمال" FieldName="demal" 
                                        VisibleIndex="11" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" EnableRowHotTrack="True" />
                                <SettingsPager>
                                    <Summary AllPagesText="صفحات : {0} - {1} ({2} مورد )" 
                                        Text="صفحه {0} از {1} ({2} مورد)" />
                                </SettingsPager>
                                <SettingsText EmptyDataRow="داده ای برای نمایش وجود ندارد" />
                                <Styles>
                                    <AlternatingRow Enabled="True">
                                    </AlternatingRow>
                                </Styles>
                            </dx:ASPxGridView>
                        </DetailRow>
                    </Templates>
                </dx:ASPxGridView>
                <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Lable"></asp:Label>
                <asp:SqlDataSource ID="Mastersqldperson" runat="server" ConnectionString="<%$ ConnectionStrings:dbsabtveladatConnectionString %>"
                    
                                   
                    
                    SelectCommand="SELECT addr, addruseremal, addrusersabt, bakhsh, bdate, bdfather, bdmother, bimarestan, cityvilage, codemahaluseremal, codemahalusersabt, codemsokonat, codemtavalod, codeposti, demal, dsabt, emal, father, fatherfname, fathername, fnameuseremal, fnameusersabt, houre, idmahaluseremal, idmahalusersabt, idmsokonat, idmtavalod, idperson, iduseremal, idusersabt, khanebehdasht, lnameuseremal, lnameusersabt, minute, mob1, mob2, mother, motherfname, mothername, name, namemahaluseremal, namemahalusersabt, namemsokonat, namemtavalod, nameuseremal, nameusersabt, ncfather, ncmother, nmtavalod, ostan, passworduseremal, passwordusersabt, sematuseremal, sematusersabt, seri, serial, sex, shahrestan, shomare, tell, telluseremal, tellusersabt, usernameuseremal, usernameusersabt, jfather, jmother FROM vperson WHERE (idusersabt = @iduser) AND (emal &lt;&gt; 0) AND (dsabt &gt;= @datesabtfrom) AND (dsabt &lt;= @datesabtto) OR (emal &lt;&gt; 0) AND (dsabt &gt;= @datesabtfrom) AND (dsabt &lt;= @datesabtto) AND (iduseremal = @iduser)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="" Name="iduser" />
                        <asp:Parameter Name="datesabtfrom" />
                        <asp:Parameter Name="datesabtto" />
                    </SelectParameters>
                </asp:SqlDataSource>
    
                <asp:SqlDataSource ID="Detailsqldperson" runat="server" ConnectionString="<%$ ConnectionStrings:dbsabtveladatConnectionString %>"
                    
                                   
                    SelectCommand="SELECT addr, addruseremal, addrusersabt, bakhsh, bdate, bdfather, bdmother, bimarestan, cityvilage, codemahaluseremal, codemahalusersabt, codemsokonat, codemtavalod, codeposti, demal, dsabt, emal, father, fatherfname, fathername, fnameuseremal, fnameusersabt, houre, idmahaluseremal, idmahalusersabt, idmsokonat, idmtavalod, idperson, iduseremal, idusersabt, khanebehdasht, lnameuseremal, lnameusersabt, minute, mob1, mob2, mother, motherfname, mothername, name, namemahaluseremal, namemahalusersabt, namemsokonat, namemtavalod, nameuseremal, nameusersabt, ncfather, ncmother, nmtavalod, ostan, passworduseremal, passwordusersabt, sematuseremal, sematusersabt, seri, serial, sex, shahrestan, shomare, tell, telluseremal, tellusersabt, usernameuseremal, usernameusersabt, jfather, jmother FROM vperson WHERE (idperson = @idperson)">
                    <SelectParameters>
                        <asp:SessionParameter Name="idperson" SessionField="idperson" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
            </div>
            </div>
<div class="col-lg-12">
</div>                
            </div>
            </div>

</asp:Content>