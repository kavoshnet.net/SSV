﻿using System;
using System.Web.UI;
using dstuserTableAdapters;

public partial class Register : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        Label5.Text = "";
        if ((Session["VOROD"] != null) && (Session["ISADMIN"] != null))
        {
            if ((Session["VOROD"].ToString() != "ok") || (Session["ISADMIN"].ToString() != "0"))
            {
                Label5.Text = "شما اجازه استفاده از این بخش را ندارید";
                Response.Redirect("Default.aspx");
            }
        }
        else
        {
            Response.Redirect("login.aspx");
        }
    }

    protected void btnInser_Click(object sender, EventArgs e)
    {
        try
        {
            string ss = ddlmahal.SelectedValue;
            var sda = new tuserTableAdapter();
            sda.Insert(int.Parse(ddlmahal.SelectedValue), txtFname.Text, txtLname.Text,
                Convert.ToBoolean(rblsex.SelectedValue), txtSemat.Text, txtmkhedmat.Text, txtAddress.Text, txtTell.Text,
                txtUserName.Text.Trim(), txtPassword.Text.Trim(),1);
            Response.Redirect("FinishRegister.aspx");
        }
        catch (Exception ex)
        {
            Label5.Text = ex.Message;
            Label5.Text = "در ثبت داده ها خطایی رخ داده است لطفا مجددا تلاش کنید.";
        }
    }
}