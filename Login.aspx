﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
         CodeFile="Login.aspx.cs" Inherits="Login" Title="صفحه ورود به سامانه" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"><p class="panel-info" style="color: darkgreen" ></p>
            </div>
            <div class="panel-body">

    
    <table style="width: 336px" align="center">
        <tr>
            <td align="center" colspan="2">
                <strong>ورود به سایت</strong></td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <img src="Images/signin.png" alt=""/></td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="Label3" runat="server" ForeColor="#C00000"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label2" runat="server" Text="نام کاربری:"></asp:Label>
            </td>
            <td align="right" style="width: 207px" dir="ltr">
                <asp:TextBox ID="txtUserName" runat="server" Width="190px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label1" runat="server" Text="کلمه عبور:"></asp:Label>
            </td>
            <td align="right" style="width: 207px" dir="ltr">
                <asp:TextBox ID="txtPass" runat="server" TextMode="Password" Width="190px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="center">
                &nbsp;</td>
            <td align="right" style="width: 207px">
                <asp:Button ID="btnLogin" runat="server" CssClass="btn btn-primary btn-xs" OnClick="btnLogin_Click"
                            Text="ورود" Width="79px" />
            </td>
        </tr>
    </table>
                </div>
            </div>
            </div>
<div class="col-lg-12">
</div>                
            </div>
            </div>

</asp:Content>