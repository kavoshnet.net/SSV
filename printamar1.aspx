﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="printamar1.aspx.cs" Inherits="printamar1" %>

<%@ Register assembly="FastReport.Web, Version=2015.1.2.0, Culture=neutral, PublicKeyToken=417583d16d08abed" namespace="FastReport.Web" tagprefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <cc1:WebReport ID="WebReport1" runat="server" ReportResourceString="77u/PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjxSZXBvcnQgU2NyaXB0TGFuZ3VhZ2U9IkNTaGFycCIgUmVwb3J0SW5mby5DcmVhdGVkPSIwMy8wMy8yMDE1IDEyOjMzOjIwIiBSZXBvcnRJbmZvLk1vZGlmaWVkPSIwMy8wMy8yMDE1IDEyOjMzOjIzIiBSZXBvcnRJbmZvLkNyZWF0b3JWZXJzaW9uPSIyMDE1LjEuMi4wIj4NCiAgPERpY3Rpb25hcnkvPg0KICA8UmVwb3J0UGFnZSBOYW1lPSJQYWdlMSI+DQogICAgPFJlcG9ydFRpdGxlQmFuZCBOYW1lPSJSZXBvcnRUaXRsZTEiIFdpZHRoPSI3MTguMiIgSGVpZ2h0PSIzNy44Ii8+DQogICAgPFBhZ2VIZWFkZXJCYW5kIE5hbWU9IlBhZ2VIZWFkZXIxIiBUb3A9IjQxLjgiIFdpZHRoPSI3MTguMiIgSGVpZ2h0PSIyOC4zNSIvPg0KICAgIDxEYXRhQmFuZCBOYW1lPSJEYXRhMSIgVG9wPSI3NC4xNSIgV2lkdGg9IjcxOC4yIiBIZWlnaHQ9Ijc1LjYiLz4NCiAgICA8UGFnZUZvb3RlckJhbmQgTmFtZT0iUGFnZUZvb3RlcjEiIFRvcD0iMTUzLjc1IiBXaWR0aD0iNzE4LjIiIEhlaWdodD0iMTguOSIvPg0KICA8L1JlcG9ydFBhZ2U+DQo8L1JlcG9ydD4NCg==" AutoWidth="True" Width="100%" Height="100%" ReportDataSources="rpdv" ShowRefreshButton="False" />
    
    </div>
    </form>
</body>
</html>
