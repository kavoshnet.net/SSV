﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
         CodeFile="RegisterUser.aspx.cs" Inherits="Register" Title="ثبت کاربر" %>

<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"><p class="panel-info" style="color: darkgreen" >لیست متولدین اداره ثبت احوال  <%= this.Session["NAMEMAHAL"].ToString()%></p>
            </div>
            <div class="panel-body">
    <table style="height: 100%; width: 100%;" align="center">
        <tr>
            <td colspan="3" style="height: 10px">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="Label5" runat="server" ForeColor="#C00000"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 20%">
                &nbsp;
            </td>
            <td style="width: 470px">
                <table border="0" style="font-family: Verdana; font-size: 100%; width: 435px;">
                    <tr>
                        <td align="center" colspan="2" style="background-color: #507cd1; color: white;">
                            <span style="font-size: 10pt" class="lbl">عضویت در سایت</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 486px">
                            <asp:Label ID="Label6" runat="server" AssociatedControlID="txtFname" CssClass="lbl"
                                       EnableTheming="True">کدشهرستان:</asp:Label>
                        </td>
                        <td align="right">
                            <asp:SqlDataSource ID="sqldmahal" runat="server" ConnectionString="<%$ ConnectionStrings:dbsabtveladatConnectionString %>"
                                               SelectCommand="SELECT * FROM [tmahal]"></asp:SqlDataSource>
                            <asp:DropDownList ID="ddlmahal" runat="server" DataSourceID="sqldmahal" DataTextField="namemahal"
                                              DataValueField="idmahal" Width="170px" CssClass="ddl">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label1" runat="server" AssociatedControlID="txtFname" CssClass="lbl"
                                       EnableTheming="True">نام:</asp:Label>
                        </td>
                        <td align="right">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserName"
                                                        Display="None" ErrorMessage="لطفا نام را وارد نمایید" Enabled="False"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtFname" runat="server" CssClass="txt" Width="170px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label2" runat="server" AssociatedControlID="txtLname" CssClass="lbl">نام خانوادگی:</asp:Label>
                        </td>
                        <td align="right">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFname"
                                                        Display="None" ErrorMessage="نام خانوادگی را وارد نمایید" Enabled="False"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtLname" runat="server" CssClass="txt" Width="170px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="UserNameLabel0" runat="server" AssociatedControlID="txtUserName" 
                                       CssClass="lbl">جنسیت:</asp:Label>
                        </td>
                        <td align="right">
                            <asp:RadioButtonList ID="rblsex" runat="server" RepeatDirection="Horizontal" 
                                                 Width="93px" Font-Names="Tahoma">
                                <asp:ListItem Value="True">مرد</asp:ListItem>
                                <asp:ListItem Value="False">زن</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="txtUserName" CssClass="lbl">نام کاربری:</asp:Label>
                        </td>
                        <td align="right">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtUserName"
                                                        Display="None" ErrorMessage="نام کاربری را وارد نمایید" Enabled="False"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtUserName" runat="server" Width="170px" CssClass="txt"></asp:TextBox>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="txtPassword" CssClass="lbl">کلمه عبور:</asp:Label>
                        </td>
                        <td align="right">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPassword"
                                                        Display="None" ErrorMessage="کلمه عبور را وارد نمایید" Enabled="False"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="170px" CssClass="txt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="txtConfirmPassword"
                                       CssClass="lbl">تکرارکلمه عبور:</asp:Label>
                        </td>
                        <td align="right">
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword"
                                                  ControlToValidate="txtConfirmPassword" Display="None" 
                                                  ErrorMessage="کلمه عبور با تکرار آن مطابقت ندارد" Enabled="False"></asp:CompareValidator>
                            <br />
                            <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" Width="170px"
                                         CssClass="txt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="txtSemat" CssClass="lbl">سمت:</asp:Label>
                        </td>
                        <td align="right">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtSemat"
                                                        Display="None" ErrorMessage="سمت را وارد" Enabled="False"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtSemat" runat="server" Width="170px" CssClass="txt"></asp:TextBox>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label7" runat="server" AssociatedControlID="txtTell" 
                                       CssClass="lbl">محل خدمت:</asp:Label>
                        </td>
                        <td align="right">
                            <asp:TextBox ID="txtmkhedmat" runat="server" Width="170px" CssClass="txt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label3" runat="server" AssociatedControlID="txtTell" CssClass="lbl">تلفن:</asp:Label>
                        </td>
                        <td align="right">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtTell"
                                                        Display="None" ErrorMessage="تلفن را وارد کنید" Enabled="False"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtTell" runat="server" Width="170px" CssClass="txt"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label4" runat="server" AssociatedControlID="txtAddress" CssClass="lbl">آدرس:</asp:Label>
                        </td>
                        <td align="right" style="width: 181px">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtAddress"
                                                        Display="None" ErrorMessage="آدرس را وارد کنید" Enabled="False"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtAddress" runat="server" CssClass="txt" TextMode="MultiLine" Width="319px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" style="color: red">
                            &nbsp;<asp:Button ID="btnInser" runat="server" CssClass="btn btn-info btn-xs" Text="ثبت عضویت" Width="123px"
                                              OnClick="btnInser_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" style="color: red">
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 20%">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                       ShowSummary="False" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <dx:ASPxGridView ID="MasterASPxdgperson" runat="server" AutoGenerateColumns="False" 
                    EnableTheming="True" KeyFieldName="iduser" RightToLeft="True" Theme="Glass" 
                    Width="100%"  DataSourceID="SqlDUsers">

                    <Columns>
                        <dx:GridViewCommandColumn VisibleIndex="0">
                            <EditButton Visible="True">
                            </EditButton>
                            <DeleteButton Visible="True">
                            </DeleteButton>
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="namemahal" VisibleIndex="1" 
                            Caption="محل خدمت">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="codemahal" VisibleIndex="2" 
                            Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="iduser" ReadOnly="True" VisibleIndex="3" 
                            Visible="False">
                            <EditFormSettings Visible="False" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="idmahal" VisibleIndex="4" Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="fname" VisibleIndex="5" Caption="نام">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="lname" VisibleIndex="6" 
                            Caption="نام خانوادگی">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataCheckColumn FieldName="sex" VisibleIndex="7" Caption="جنسیت">
                        </dx:GridViewDataCheckColumn>
                        <dx:GridViewDataTextColumn FieldName="semat" VisibleIndex="8" Caption="سمت">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="mkhedmat" VisibleIndex="9" 
                            Caption="محل خدمت">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="addr" VisibleIndex="10" Caption="آدرس">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="tell" VisibleIndex="11" Caption="تلفن">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="username" VisibleIndex="12" 
                            Caption="نام کاربری">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="password" VisibleIndex="13" 
                            Caption="رمز عبور">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="isadmin" VisibleIndex="14" 
                            Caption="کاربر اصلی">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <SettingsBehavior AllowFocusedRow="True" EnableRowHotTrack="True" />

                    <SettingsPager>
                        <Summary AllPagesText="صفحات : {0} - {1} ({2} مورد )" 
                            Text="صفحه {0} از {1} ({2} مورد)" />
                    </SettingsPager>

                    <Settings ShowFilterRow="True" />
                    <SettingsText EmptyDataRow="داده ای برای نمایش وجود ندارد" />

                    <Styles>
                        <AlternatingRow Enabled="True">
                        </AlternatingRow>
                    </Styles>
                </dx:ASPxGridView>
                <asp:SqlDataSource ID="SqlDUsers" runat="server" 
                                   ConnectionString="<%$ ConnectionStrings:dbsabtveladatConnectionString %>" 
                                   SelectCommand="SELECT * FROM [vuser]">
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
                </div>
            </div>
            </div>
<div class="col-lg-12">
</div>                
            </div>
            </div>

</asp:Content>