﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SabtGovahi.aspx.cs" Inherits="SabtGovahi" Title="ثبت گواهی ولادت" %>


<%@ Register Assembly="JQControls" Namespace="JQControls" TagPrefix="cc1" %>


<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        $(document).ready(function () {
<%--            $("#btnrep1").click(function () {
            //$("#<%=btnrep.ClientID %>").click(function () {
                setTimeout(function () { document.location = "sabtgovahi.aspx"; }, 500);
                $("#myTab li:eq(1) a").tab("show");
                alert("aaaaaaaaaaaa");
            });--%>
        });
    </script>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist" id="myTab">
                        <li role="presentation" class="active"><a href="#sabt" aria-controls="sabt" role="tab" data-toggle="tab">ثبت ولادت</a></li>
                        <li role="presentation" ><a href="#display" aria-controls="display" role="tab" data-toggle="tab">نمایش ولادتهای ثبت شده</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="sabt">


                            <div class="panel-body">
                                <asp:Label ID="lblError" runat="server" ForeColor="#CC0000" Text="Label"></asp:Label>
                                <br />
                                <asp:Label ID="lblBDError" runat="server" ForeColor="#CC0000" Text="Label" Visible="False"></asp:Label>
                                <div>
                                    <fieldset>
                                        <legend><b>مشخصات صادر کننده گواهی </b></legend>
                                        <table class="tbl" align="center">
                                            <tr>
                                                <td style="width: 104px">
                                                    <asp:Label ID="Label25" runat="server" Text="اینجانب نام : "></asp:Label>
                                                </td>
                                                <td style="width: 133px">
                                                    <asp:TextBox ID="txtfname" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 86px">
                                                    <asp:Label ID="Label26" runat="server" Text="نام خانوادگی : "></asp:Label>
                                                </td>
                                                <td style="width: 153px">
                                                    <asp:TextBox ID="txtlname" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 135px">
                                                    <asp:Label ID="Label27" runat="server" Text="سمت : "></asp:Label>
                                                </td>
                                                <td dir="rtl">
                                                    <asp:RadioButtonList ID="rblsemat" runat="server"
                                                        RepeatDirection="Horizontal" TextAlign="Left" Width="183px">
                                                        <asp:ListItem Value="1">پزشک</asp:ListItem>
                                                        <asp:ListItem Value="2">ماما</asp:ListItem>
                                                        <asp:ListItem Value="3">بهورز</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 104px">
                                                    <asp:Label ID="Label28" runat="server" Text="محل خدمت : "></asp:Label>
                                                </td>
                                                <td style="width: 133px">
                                                    <asp:TextBox ID="txtmkhedmat" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 86px">
                                                    <asp:Label ID="Label29" runat="server" Text="به نشانی : "></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="txtaddr" runat="server" Width="509px"></asp:TextBox>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="width: 104px">
                                                    <asp:Label ID="Label51" runat="server" Text="شماره پرونده:"></asp:Label>
                                                </td>
                                                <td style="width: 133px">
                                                    <asp:TextBox ID="txtshomare" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 86px">
                                                    <asp:Label ID="Label52" runat="server" Text="سری گواهی:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtseri" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 135px">
                                                    <asp:Label ID="Label53" runat="server" Text="سریال گواهی:"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtserial" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                    </fieldset>
                                </div>
                                <br />
                                <div>
                                    <fieldset>
                                        <legend><b>نام،جنسیت،تاریخ و محل تولد نوزاد </b></legend>
                                        <table class="tbl" align="right">
                                            <tr>
                                                <td style="width: 90px">
                                                    <asp:Label ID="Label2" runat="server" Text="نام : "></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 126px">
                                                    <asp:Label ID="Label3" runat="server" Text="جنسیت : "></asp:Label>
                                                </td>
                                                <td colspan="1" style="width: 54px" class="dxeButtonEditSys">
                                                    <asp:RadioButtonList ID="rblsex" runat="server"
                                                        RepeatDirection="Horizontal" TextAlign="Left" Width="124px">
                                                        <asp:ListItem Value="True">پسر</asp:ListItem>
                                                        <asp:ListItem Value="False">دختر</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td style="width: 95px" colspan="1" class="dxeButtonEditSys">
                                                    <asp:Label ID="Label4" runat="server" Text="تاریخ : "></asp:Label>
                                                </td>
                                                <td colspan="4" style="width: 308px">
                                                    <cc1:JQLoader ID="JQLoader1" runat="server" />
                                                    <cc1:JQDatePicker ID="jqdbdate" runat="server" CssClass="txtl" DateFormat="YMD"
                                                        Regional="fa"></cc1:JQDatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 90px">
                                                    <asp:Label ID="Label5" runat="server" Text="ساعت : "></asp:Label>
                                                </td>
                                                <td style="width: 37px">
                                                    <asp:TextBox ID="txthoure" runat="server" Width="35px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label46" runat="server" Text="دقیقه : "></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtminute" runat="server" Width="35px"></asp:TextBox>
                                                </td>
                                                <td style="width: 126px">
                                                    <asp:Label ID="Label10" runat="server" Text="در بیمارستان : "></asp:Label>
                                                </td>
                                                <td style="width: 54px" class="dxeButtonEditSys">
                                                    <asp:TextBox ID="txtbimarestan" runat="server" Width="100px"></asp:TextBox>
                                                    &nbsp;</td>
                                                <td colspan="1" style="width: 95px" class="dxeButtonEditSys">
                                                    <asp:Label ID="Label42" runat="server" Text="در خانه بهداشت : "></asp:Label>
                                                </td>
                                                <td colspan="1" style="width: 38px">
                                                    <asp:TextBox ID="txtkhanebehdasht" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label59" runat="server" Text="مکان تولد:"></asp:Label>
                                                </td>
                                                <td colspan="2">
                                                    <asp:RadioButtonList ID="rblnmtavaload" runat="server"
                                                        RepeatDirection="Horizontal" TextAlign="Left"
                                                        Width="150px" Font-Bold="False" Font-Names="Tahoma" Font-Size="X-Small">
                                                        <asp:ListItem Value="1">منزل</asp:ListItem>
                                                        <asp:ListItem Value="2">سایر</asp:ListItem>
                                                        <asp:ListItem Value="0">هیچکدام</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 90px">
                                                    <asp:Label ID="Label31" runat="server" Text="واقع در استان : "></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="txtostan" runat="server" Height="20px" Width="120px"></asp:TextBox>
                                                </td>
                                                <td style="width: 126px">
                                                    <asp:Label ID="Label32" runat="server" Text="شهرستان : "></asp:Label>
                                                </td>
                                                <td style="width: 54px" class="dxeButtonEditSys">
                                                    <asp:DropDownList ID="ddlmtavalod" runat="server" CssClass="ddl" DataSourceID="sqldmtavalod"
                                                        DataTextField="namemahal" DataValueField="idmahal" Width="105px">
                                                    </asp:DropDownList>
                                                    <asp:SqlDataSource ID="sqldmtavalod" runat="server" ConnectionString="<%$ ConnectionStrings:dbsabtveladatConnectionString %>"
                                                        SelectCommand="SELECT idmahal, namemahal, codemahal FROM tmahal WHERE (idmahal &lt;&gt; 0)"></asp:SqlDataSource>
                                                </td>
                                                <td colspan="1" style="width: 95px" class="dxeButtonEditSys">
                                                    <asp:Label ID="Label33" runat="server" Text="بخش : "></asp:Label>
                                                </td>
                                                <td colspan="1" style="width: 38px">
                                                    <asp:TextBox ID="txtbakhsh" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 158px" colspan="2">
                                                    <asp:Label ID="Label34" runat="server" Text="شهر / روستا : "></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtcityvilage" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 90px">
                                                    <br />
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                    </fieldset>
                                </div>
                                <br />

                                <div>
                                    <fieldset>
                                        <legend><b>مشخصات پدر نوزاد </b></legend>
                                        <table class="tbl" align="right">
                                            <tr>
                                                <td style="width: 104px">
                                                    <asp:Label ID="Label12" runat="server" Text="نام : "></asp:Label>
                                                </td>
                                                <td style="width: 133px">
                                                    <asp:TextBox ID="txtfathername" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 86px">
                                                    <asp:Label ID="Label13" runat="server" Text="نام خانوادگی : "></asp:Label>
                                                </td>
                                                <td style="width: 153px">
                                                    <asp:TextBox ID="txtfatherfname" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 104px">
                                                    <asp:Label ID="Label43" runat="server" Text="شماره ملی : "></asp:Label>
                                                </td>
                                                <td dir="rtl">
                                                    <asp:TextBox ID="txtncfather" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 104px">
                                                    <asp:Label ID="Label36" runat="server" Text="تاریخ تولد : "></asp:Label>
                                                </td>
                                                <td colspan="5">
                                                    <cc1:JQDatePicker ID="jqdbdfather" runat="server" CssClass="txtl"
                                                        DateFormat="YMD" Regional="fa"></cc1:JQDatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <br />
                                                </td>
                                            </tr>

                                        </table>
                                        <br />
                                    </fieldset>
                                </div>
                                <br />
                                <div>
                                    <fieldset>
                                        <legend><b>مشخصات مادر نوزاد </b></legend>
                                        <table class="tbl" align="right">
                                            <tr>
                                                <td style="width: 104px">
                                                    <asp:Label ID="Label35" runat="server" Text="نام : "></asp:Label>
                                                </td>
                                                <td style="width: 133px">
                                                    <asp:TextBox ID="txtmothername" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 86px">
                                                    <asp:Label ID="Label37" runat="server" Text="نام خانوادگی : "></asp:Label>
                                                </td>
                                                <td style="width: 153px">
                                                    <asp:TextBox ID="txtmotherfname" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 104px">
                                                    <asp:Label ID="Label44" runat="server" Text="شماره ملی : "></asp:Label>
                                                </td>
                                                <td dir="rtl">
                                                    <asp:TextBox ID="txtncmother" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 104px">
                                                    <asp:Label ID="Label38" runat="server" Text="تاریخ تولد : "></asp:Label>
                                                </td>
                                                <td colspan="5">
                                                    <cc1:JQDatePicker ID="jqdbdmother" runat="server" CssClass="txtl"
                                                        DateFormat="YMD" Regional="fa"></cc1:JQDatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <br />
                                                </td>
                                            </tr>

                                        </table>
                                        <br />
                                    </fieldset>
                                </div>
                                <br />
                                <div>
                                    <fieldset>
                                        <legend><b>محل سکونت والدین </b></legend>
                                        <table class="tbl" align="right">
                                            <tr>
                                                <td style="width: 104px">
                                                    <asp:Label ID="Label39" runat="server" Text="واقع در استان : "></asp:Label>
                                                </td>
                                                <td style="width: 133px">
                                                    <asp:TextBox ID="txtostansokonat" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 86px">
                                                    <asp:Label ID="Label40" runat="server" Text="شهرستان : "></asp:Label>
                                                </td>
                                                <td style="width: 153px" colspan="2">
                                                    <asp:DropDownList ID="ddlmsokonat" runat="server" CssClass="ddl" DataSourceID="sqldmsokonat"
                                                        DataTextField="namemahal" DataValueField="idmahal" Width="105px">
                                                    </asp:DropDownList>
                                                    <asp:SqlDataSource ID="sqldmsokonat" runat="server" ConnectionString="<%$ ConnectionStrings:dbsabtveladatConnectionString %>"
                                                        SelectCommand="SELECT idmahal, namemahal, codemahal FROM tmahal WHERE (idmahal &lt;&gt; 0)"></asp:SqlDataSource>
                                                </td>
                                                <td style="width: 55px">
                                                    <asp:Label ID="Label45" runat="server" Text="بخش : "></asp:Label>
                                                </td>
                                                <td dir="rtl">
                                                    <asp:TextBox ID="txtbakhshsokonat" runat="server"></asp:TextBox>
                                                </td>
                                                <td dir="rtl">
                                                    <asp:Label ID="Label47" runat="server" Text="شهر / روستا : "></asp:Label>
                                                </td>
                                                <td dir="rtl">
                                                    <asp:TextBox ID="txtcityvilagesokonat" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 104px">
                                                    <asp:Label ID="Label50" runat="server" Text="آدرس"></asp:Label>
                                                </td>
                                                <td colspan="8">
                                                    <asp:TextBox ID="txtaddrsokonat" runat="server" Width="648px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 104px">
                                                    <asp:Label ID="Label41" runat="server" Text="کد پستی : "></asp:Label>
                                                </td>
                                                <td colspan="1">
                                                    <asp:TextBox ID="txtcodeposti" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label48" runat="server" Text="تلفن منزل : "></asp:Label>
                                                </td>
                                                <td colspan="1">
                                                    <asp:TextBox ID="txttell" runat="server"></asp:TextBox>
                                                </td>
                                                <td colspan="2">
                                                    <asp:Label ID="Label49" runat="server" Text="موبایل : "></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="txtmob1" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <br />
                                                </td>
                                            </tr>

                                        </table>
                                        <br />
                                    </fieldset>
                                </div>
                                <br />
                                <table class="tbl">
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="btnnew" runat="server" CssClass="btn btn-info btn-xs"
                                                OnClick="btnnew_Click" Text="جدید" CausesValidation="False" />
                                            <asp:Button ID="btninsert" runat="server" CssClass="btn btn-info btn-xs" OnClick="btnInsert_Click" Text="ذخیره" />
                                            <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-info btn-xs" OnClick="Cancel_Click" Text="انصراف"
                                                CausesValidation="False" />
                                            <asp:Button ID="btndelete" runat="server" CssClass="btn btn-info btn-xs" OnClick="btnDelete_Click" Text="حذف" CausesValidation="False" OnClientClick=" return confirm('آیا برای حذف اطمینان دارید') " />
                                            <asp:Button ID="btnprint" runat="server" CssClass="btn btn-info btn-xs"
                                                OnClick="btnprint_Click" Text="چاپ" CausesValidation="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 15px" align="center">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade in" id="display">
                            <div class="panel-body">
                                <table class="nav-justified">
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="Label54" runat="server" Text="از تاریخ: "></asp:Label>
                                            <cc1:JQDatePicker ID="jqdrdatef" runat="server" CssClass="txtl" DateFormat="YMD"
                                                Regional="fa"></cc1:JQDatePicker>
                                            <asp:Label ID="Label56" runat="server" Text="تا تاریخ: "></asp:Label>
                                            <cc1:JQDatePicker ID="jqdrdatet" runat="server" CssClass="txtl" DateFormat="YMD"
                                                Regional="fa"></cc1:JQDatePicker>
                                            <asp:Button ID="btnrep" runat="server" CssClass="btn btn-danger btn-xs"
                                                OnClick="btnrep_Click" Text="اعمال فیلتر" CausesValidation="False" />
                                            <asp:Button ID="btnprn" runat="server" CssClass="btn btn-info btn-xs"
                                                OnClick="btnprn_Click" Text="چاپ اطلاعات" CausesValidation="False" />
                                            <%--<input type="button" id="btnrep1" value="اعمال فیلتر"style="width: 120px" />--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:ASPxGridView ID="MasterASPxdgperson" runat="server" AutoGenerateColumns="False"
                                                EnableTheming="True" KeyFieldName="idperson" RightToLeft="True" Theme="Glass"
                                                Width="100%" OnHtmlRowPrepared="MasterASPxdgperson_HtmlRowPrepared"
                                                DataSourceID="Mastersqldperson">

                                                <GroupSummary>
                                                    <dx:ASPxSummaryItem FieldName="mother" ShowInColumn="نام مادر"
                                                        ShowInGroupFooterColumn="نام مادر" SummaryType="Count" />
                                                </GroupSummary>

                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="نام مادر" FieldName="mother"
                                                        VisibleIndex="1">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="نام پدر" FieldName="father"
                                                        VisibleIndex="2">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="آدرس" FieldName="addr" VisibleIndex="3"
                                                        Visible="False">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="تاریخ تولد" FieldName="bdate"
                                                        VisibleIndex="4">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="تلفن" FieldName="tell" VisibleIndex="7">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="موبایل" FieldName="mob1" VisibleIndex="7">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn VisibleIndex="0" Caption="انجام" Visible="False">
                                                        <DataItemTemplate>
                                                            <asp:LinkButton ID="LinkButton3" runat="server"
                                                                OnClientClick=" return confirm('آیا برای ثبت اطمینان دارید?') "
                                                                CommandName="Emal" OnClick="LinkButton1_Click">انجام</asp:LinkButton>
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="idperson" Visible="False"
                                                        VisibleIndex="11">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="کاربر ثبت" FieldName="nameusersabt"
                                                        VisibleIndex="6" Visible="False">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="تاریخ ثبت" FieldName="dsabt"
                                                        VisibleIndex="12" Visible="False">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="مهلت" VisibleIndex="8">
                                                        <DataItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="محل سکونت" FieldName="namemsokonat"
                                                        VisibleIndex="5">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="چاپ" VisibleIndex="10">
                                                        <DataItemTemplate>
                                                            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click">چاپ</asp:LinkButton>
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="ویرایش" VisibleIndex="9">
                                                        <DataItemTemplate>
                                                            <asp:LinkButton ID="LinkButton4" runat="server" OnClick="LinkButton4_Click">ویرایش</asp:LinkButton>
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <SettingsBehavior AllowFocusedRow="True" EnableRowHotTrack="True" />

                                                <SettingsPager PageSize="20">
                                                    <Summary AllPagesText="صفحات : {0} - {1} ({2} مورد )"
                                                        Text="صفحه {0} از {1} ({2} مورد)" />
                                                </SettingsPager>

                                                <Settings ShowFilterRow="True" ShowFooter="True"
                                                    ShowGroupFooter="VisibleAlways" />
                                                <SettingsText EmptyDataRow="داده ای برای نمایش وجود ندارد" />
                                                <SettingsDetail ShowDetailRow="True" />

                                                <Styles>
                                                    <AlternatingRow Enabled="True">
                                                    </AlternatingRow>
                                                </Styles>
                                                <Templates>
                                                    <DetailRow>
                                                        <dx:ASPxGridView ID="DetailASPxdgperson" runat="server"
                                                            AutoGenerateColumns="False" EnableTheming="True" KeyFieldName="idperson" RightToLeft="True"
                                                            Theme="Office2003Blue" Width="100%" DataSourceID="Detailsqldperson"
                                                            OnBeforePerformDataSelect="DetailASPxdgperson_BeforePerformDataSelect">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="ش.م مادر" FieldName="ncmother" VisibleIndex="1"
                                                                    Visible="True">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="ش.م پدر" FieldName="ncfather" VisibleIndex="1"
                                                                    Visible="True">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="مادر" FieldName="mother" VisibleIndex="1"
                                                                    Visible="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="پدر" FieldName="father" VisibleIndex="2"
                                                                    Visible="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="آدرس" FieldName="addr" VisibleIndex="3">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="ت.ت" FieldName="bdate" VisibleIndex="4"
                                                                    Visible="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="تلفن" FieldName="tell" VisibleIndex="7"
                                                                    Visible="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="موبایل" FieldName="mob1" VisibleIndex="8"
                                                                    Visible="False">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="انجام" VisibleIndex="0" Visible="False">
                                                                    <DataItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Emal"
                                                                            OnClick="LinkButton1_Click"
                                                                            OnClientClick=" return confirm('آیا برای ثبت اطمینان دارید?') ">انجام</asp:LinkButton>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="idperson" Visible="False"
                                                                    VisibleIndex="10">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="کاربر ثبت" FieldName="nameusersabt"
                                                                    VisibleIndex="8">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="تاریخ ثبت" FieldName="dsabt"
                                                                    VisibleIndex="11">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="مهلت" VisibleIndex="6" Visible="False">
                                                                    <DataItemTemplate>
                                                                        <asp:Label ID="Label58" runat="server" Text="Label"></asp:Label>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                            </Columns>
                                                            <SettingsBehavior AllowFocusedRow="True" EnableRowHotTrack="True" />
                                                            <SettingsBehavior AllowFocusedRow="True" EnableRowHotTrack="True" />
                                                            <Styles>
                                                                <AlternatingRow Enabled="True">
                                                                </AlternatingRow>
                                                            </Styles>
                                                        </dx:ASPxGridView>
                                                    </DetailRow>
                                                </Templates>
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <asp:SqlDataSource ID="Mastersqldperson" runat="server" ConnectionString="<%$ ConnectionStrings:dbsabtveladatConnectionString %>"
                                                SelectCommand="SELECT addr, addruseremal, addrusersabt, bakhsh, bdate, bdfather, bdmother, bimarestan, cityvilage, codemahaluseremal, codemahalusersabt, codemsokonat, codemtavalod, codeposti, demal, dsabt, emal, father, fatherfname, fathername, fnameuseremal, fnameusersabt, houre, idmahaluseremal, idmahalusersabt, idmsokonat, idmtavalod, idperson, iduseremal, idusersabt, khanebehdasht, lnameuseremal, lnameusersabt, minute, mob1, mob2, mother, motherfname, mothername, name, namemahaluseremal, namemahalusersabt, namemsokonat, namemtavalod, nameuseremal, nameusersabt, ncfather, ncmother, nmtavalod, ostan, passworduseremal, passwordusersabt, sematuseremal, sematusersabt, seri, serial, sex, shahrestan, shomare, tell, telluseremal, tellusersabt, usernameuseremal, usernameusersabt, jfather, jmother FROM vperson WHERE (idusersabt = @iduser) AND (emal = 0) AND (dsabt &gt;= @datesabtfrom) AND (dsabt &lt;= @datesabtto)">
                                                <SelectParameters>
                                                    <asp:Parameter Name="iduser" />
                                                    <asp:Parameter Name="datesabtfrom" />
                                                    <asp:Parameter Name="datesabtto" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                            <asp:SqlDataSource ID="Detailsqldperson" runat="server" ConnectionString="<%$ ConnectionStrings:dbsabtveladatConnectionString %>"
                                                SelectCommand="SELECT addr, addruseremal, addrusersabt, bakhsh, bdate, bdfather, bdmother, bimarestan, cityvilage, codemahaluseremal, codemahalusersabt, codemsokonat, codemtavalod, codeposti, demal, dsabt, emal, father, fatherfname, fathername, fnameuseremal, fnameusersabt, houre, idmahaluseremal, idmahalusersabt, idmsokonat, idmtavalod, idperson, iduseremal, idusersabt, khanebehdasht, lnameuseremal, lnameusersabt, minute, mob1, mob2, mother, motherfname, mothername, name, namemahaluseremal, namemahalusersabt, namemsokonat, namemtavalod, nameuseremal, nameusersabt, ncfather, ncmother, nmtavalod, ostan, passworduseremal, passwordusersabt, sematuseremal, sematusersabt, seri, serial, sex, shahrestan, shomare, tell, telluseremal, tellusersabt, usernameuseremal, usernameusersabt, jfather, jmother FROM vperson WHERE (idperson = @idperson)">
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="idperson" SessionField="idperson" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>

                                </table>




                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
