﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BijanComponents;
using DevExpress.Utils.OAuth.Provider;
using FastReport;
using FastReport.Data;

public partial class printamar1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Session["VOROD"] == null))
            {
                Response.Redirect("Login.aspx");
            }
        else if ((Session["VOROD"].ToString() != "ok"))
            {
                Response.Redirect("Login.aspx");
            }
            string connectionstr = (string)(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["dbsabtveladatConnectionString"].ConnectionString).ToString();

                //generalfunc.get_connectionstring();
            var rpt = new Report();
            //conection string with parameter------------------------------------------------------
            rpt.Load(Server.MapPath("~/APP_DATA/amar1.frx"));
            rpt.SetParameterValue("ConnectionStr", connectionstr);
            if (rpt.Dictionary.Connections.Count > 0)
            {
                rpt.Dictionary.Connections[0].ConnectionString = connectionstr;
                rpt.Dictionary.Connections[0].ConnectionStringExpression = "[ConnectionStr]";
                rpt.Dictionary.Connections[0].CommandTimeout = 60;
            }


            string DateNow = "از تاریخ " + Session["fromdate"].ToString() + " تا تاریخ " + Session["todate"].ToString();//ShamsiDate.GetShamsiDate(DateTime.Now).Trim();
            rpt.SetParameterValue("datenow", DateNow);
            string onvan = "";
            if (Session["fromdate"] != null && Session["todate"] != null)
            {
                //load data
                var data = rpt.GetDataSource("tperson") as TableDataSource;
                    data.SelectCommand = string.Format("SELECT * from tperson where dsabt >='{0}' and dsabt <='{1}' order by idperson asc",
                        Session["fromdate"].ToString(), Session["todate"].ToString());
                    onvan = "آمار ولادتهای " + Session["MKHEDMAT"].ToString();
            }

            //convert date to persian format with parameter-----------------------------------------------
            //------------------------------------------------------------------------------------------
            rpt.SetParameterValue("onvan", onvan);
            rpt.Prepare();
            WebReport1.Report = rpt;
            WebReport1.LocalizationFile = Server.MapPath("~/APP_DATA/Persian.frl");
            WebReport1.ReportDone = true;
        }

}