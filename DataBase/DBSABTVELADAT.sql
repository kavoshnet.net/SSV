USE [DBSABTVELADAT]
GO
/****** Object:  Table [dbo].[tmahal]    Script Date: 06/22/2019 11:03:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tmahal](
	[idmahal] [int] IDENTITY(1,1) NOT NULL,
	[namemahal] [nvarchar](50) NULL,
	[codemahal] [int] NULL,
 CONSTRAINT [PK_tmahal] PRIMARY KEY CLUSTERED 
(
	[idmahal] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tuser]    Script Date: 06/22/2019 11:03:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tuser](
	[iduser] [int] IDENTITY(1,1) NOT NULL,
	[idmahal] [int] NULL,
	[fname] [nvarchar](50) NULL,
	[lname] [nvarchar](50) NULL,
	[sex] [bit] NULL,
	[semat] [nvarchar](50) NULL,
	[mkhedmat] [nvarchar](250) NULL,
	[addr] [text] NULL,
	[tell] [nvarchar](50) NULL,
	[username] [nvarchar](20) NULL,
	[password] [nvarchar](20) NULL,
	[isadmin] [int] NULL,
 CONSTRAINT [PK_tuser] PRIMARY KEY CLUSTERED 
(
	[iduser] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tperson]    Script Date: 06/22/2019 11:03:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tperson](
	[idperson] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[sex] [bit] NULL,
	[bdate] [nchar](10) NULL,
	[houre] [int] NULL,
	[minute] [int] NULL,
	[bimarestan] [nvarchar](255) NULL,
	[khanebehdasht] [nvarchar](255) NULL,
	[ostan] [nvarchar](50) NULL,
	[shahrestan] [nvarchar](50) NULL,
	[bakhsh] [nvarchar](50) NULL,
	[cityvilage] [nvarchar](50) NULL,
	[fathername] [nvarchar](50) NULL,
	[fatherfname] [nvarchar](50) NULL,
	[ncfather] [nchar](10) NULL,
	[bdfather] [nchar](10) NULL,
	[jfather] [nvarchar](50) NULL,
	[mothername] [nvarchar](50) NULL,
	[motherfname] [nvarchar](50) NULL,
	[ncmother] [nchar](10) NULL,
	[bdmother] [nchar](10) NULL,
	[jmother] [nvarchar](50) NULL,
	[idmtavalod] [int] NULL,
	[nmtavalod] [nvarchar](50) NULL,
	[ostansokonat] [nvarchar](50) NULL,
	[bakhshsokonat] [nvarchar](50) NULL,
	[cityvilagesokonat] [nvarchar](50) NULL,
	[codeposti] [nchar](10) NULL,
	[addr] [text] NULL,
	[idmsokonat] [int] NULL,
	[tell] [nchar](11) NULL,
	[mob1] [nchar](11) NULL,
	[mob2] [nchar](11) NULL,
	[dsabt] [nchar](10) NULL,
	[emal] [bit] NULL,
	[demal] [nchar](10) NULL,
	[idusersabt] [int] NULL,
	[iduseremal] [int] NULL,
	[shomare] [nvarchar](50) NULL,
	[seri] [nvarchar](3) NULL,
	[serial] [nvarchar](6) NULL,
 CONSTRAINT [PK_tperson_1] PRIMARY KEY CLUSTERED 
(
	[idperson] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[vuser]    Script Date: 06/22/2019 11:03:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vuser]
AS
SELECT     dbo.tmahal.namemahal, dbo.tmahal.codemahal, dbo.tuser.iduser, dbo.tuser.idmahal, dbo.tuser.fname, dbo.tuser.lname, dbo.tuser.sex, 
                      dbo.tuser.semat, dbo.tuser.mkhedmat, dbo.tuser.addr, dbo.tuser.tell, dbo.tuser.username, dbo.tuser.password, dbo.tuser.isadmin
FROM         dbo.tuser INNER JOIN
                      dbo.tmahal ON dbo.tuser.idmahal = dbo.tmahal.idmahal
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[11] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tuser"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 190
            End
            DisplayFlags = 280
            TopColumn = 5
         End
         Begin Table = "tmahal"
            Begin Extent = 
               Top = 6
               Left = 228
               Bottom = 106
               Right = 380
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vuser'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vuser'
GO
/****** Object:  View [dbo].[vperson]    Script Date: 06/22/2019 11:03:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vperson]
AS
SELECT     dbo.tperson.idperson, tmahaltavalod.codemahal AS codemtavalod, tmahaltavalod.namemahal AS namemtavalod, 
                      tmahalsokonat.codemahal AS codemsokonat, tmahalsokonat.namemahal AS namemsokonat, tusersabt.idmahal AS idmahalusersabt, 
                      tusersabt.fname AS fnameusersabt, tusersabt.lname AS lnameusersabt, tusersabt.fname + ' ' + tusersabt.lname AS nameusersabt, 
                      tusersabt.tell AS tellusersabt, tusersabt.addr AS addrusersabt, tusersabt.semat AS sematusersabt, tusersabt.username AS usernameusersabt, 
                      tusersabt.password AS passwordusersabt, tmahalusersabt.codemahal AS codemahalusersabt, tmahalusersabt.namemahal AS namemahalusersabt, 
                      tuseremal.idmahal AS idmahaluseremal, tuseremal.fname AS fnameuseremal, tuseremal.lname AS lnameuseremal, 
                      tuseremal.fname + ' ' + tuseremal.lname AS nameuseremal, tuseremal.tell AS telluseremal, tuseremal.addr AS addruseremal, 
                      tuseremal.semat AS sematuseremal, tuseremal.username AS usernameuseremal, tuseremal.password AS passworduseremal, 
                      tmahaluseremal.namemahal AS namemahaluseremal, tmahaluseremal.codemahal AS codemahaluseremal, dbo.tperson.name, dbo.tperson.sex, 
                      dbo.tperson.bdate, dbo.tperson.houre, dbo.tperson.minute, dbo.tperson.bimarestan, dbo.tperson.khanebehdasht, dbo.tperson.ostan, 
                      dbo.tperson.shahrestan, dbo.tperson.bakhsh, dbo.tperson.cityvilage, dbo.tperson.fathername, dbo.tperson.fatherfname, dbo.tperson.ncfather, 
                      dbo.tperson.bdfather, dbo.tperson.fathername + ' ' + dbo.tperson.fatherfname AS father, dbo.tperson.mothername, dbo.tperson.motherfname, 
                      dbo.tperson.ncmother, dbo.tperson.bdmother, dbo.tperson.mothername + ' ' + dbo.tperson.motherfname AS mother, dbo.tperson.idmtavalod, 
                      dbo.tperson.nmtavalod, dbo.tperson.codeposti, dbo.tperson.addr, dbo.tperson.idmsokonat, dbo.tperson.tell, dbo.tperson.mob1, dbo.tperson.mob2, 
                      dbo.tperson.dsabt, dbo.tperson.emal, dbo.tperson.demal, dbo.tperson.idusersabt, dbo.tperson.iduseremal, tusersabt.sex AS sexusersabt, 
                      tusersabt.mkhedmat AS mkhedmatusersabt, tusersabt.isadmin, dbo.tperson.shomare, dbo.tperson.seri, dbo.tperson.serial, dbo.tperson.jfather, 
                      dbo.tperson.jmother
FROM         dbo.tuser AS tuseremal INNER JOIN
                      dbo.tmahal AS tmahaluseremal ON tuseremal.idmahal = tmahaluseremal.idmahal RIGHT OUTER JOIN
                      dbo.tmahal AS tmahaltavalod INNER JOIN
                      dbo.tperson INNER JOIN
                      dbo.tmahal AS tmahalusersabt INNER JOIN
                      dbo.tuser AS tusersabt ON tmahalusersabt.idmahal = tusersabt.idmahal ON dbo.tperson.idusersabt = tusersabt.iduser INNER JOIN
                      dbo.tmahal AS tmahalsokonat ON dbo.tperson.idmsokonat = tmahalsokonat.idmahal ON tmahaltavalod.idmahal = dbo.tperson.idmtavalod ON 
                      tuseremal.iduser = dbo.tperson.iduseremal
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[50] 4[28] 2[7] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tuseremal"
            Begin Extent = 
               Top = 210
               Left = 550
               Bottom = 386
               Right = 702
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "tmahaluseremal"
            Begin Extent = 
               Top = 105
               Left = 589
               Bottom = 206
               Right = 741
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tmahaltavalod"
            Begin Extent = 
               Top = 19
               Left = 9
               Bottom = 140
               Right = 161
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tperson"
            Begin Extent = 
               Top = 0
               Left = 360
               Bottom = 285
               Right = 512
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "tmahalusersabt"
            Begin Extent = 
               Top = 145
               Left = 0
               Bottom = 257
               Right = 152
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tusersabt"
            Begin Extent = 
               Top = 125
               Left = 183
               Bottom = 240
               Right = 335
            End
            DisplayFlags = 280
            TopColumn = 5
         End
         Begin Table = "tmahalsokonat"
            Begin Extent = 
               Top = 0
               Left = 576
               Bottom = 105
               Right = 728
            End
  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vperson'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'          DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 44
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 3750
         Table = 2295
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vperson'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vperson'
GO
/****** Object:  Default [DF_tperson_name]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_name]  DEFAULT (N'-') FOR [name]
GO
/****** Object:  Default [DF_tperson_sex]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_sex]  DEFAULT ((1)) FOR [sex]
GO
/****** Object:  Default [DF_tperson_bdate]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_bdate]  DEFAULT (N'1300/01/01') FOR [bdate]
GO
/****** Object:  Default [DF_tperson_houre]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_houre]  DEFAULT ((12)) FOR [houre]
GO
/****** Object:  Default [DF_tperson_minute]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_minute]  DEFAULT ((0)) FOR [minute]
GO
/****** Object:  Default [DF_tperson_bimarestan]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_bimarestan]  DEFAULT (N'-') FOR [bimarestan]
GO
/****** Object:  Default [DF_tperson_khanebehdasht]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_khanebehdasht]  DEFAULT (N'-') FOR [khanebehdasht]
GO
/****** Object:  Default [DF_tperson_ostan]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_ostan]  DEFAULT (N'همدان') FOR [ostan]
GO
/****** Object:  Default [DF_tperson_shahrestan]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_shahrestan]  DEFAULT (N'-') FOR [shahrestan]
GO
/****** Object:  Default [DF_tperson_bakhsh]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_bakhsh]  DEFAULT (N'مرکزی') FOR [bakhsh]
GO
/****** Object:  Default [DF_tperson_cityvilage]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_cityvilage]  DEFAULT (N'-') FOR [cityvilage]
GO
/****** Object:  Default [DF_tperson_fathername]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_fathername]  DEFAULT (N'-') FOR [fathername]
GO
/****** Object:  Default [DF_tperson_fatherfname]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_fatherfname]  DEFAULT (N'-') FOR [fatherfname]
GO
/****** Object:  Default [DF_tperson_ncfather]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_ncfather]  DEFAULT (N'0000000000') FOR [ncfather]
GO
/****** Object:  Default [DF_tperson_bdfather]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_bdfather]  DEFAULT (N'1300/01/01') FOR [bdfather]
GO
/****** Object:  Default [DF_tperson_mothername]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_mothername]  DEFAULT (N'-') FOR [mothername]
GO
/****** Object:  Default [DF_tperson_motherfname]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_motherfname]  DEFAULT (N'-') FOR [motherfname]
GO
/****** Object:  Default [DF_tperson_ncmother]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_ncmother]  DEFAULT (N'0000000000') FOR [ncmother]
GO
/****** Object:  Default [DF_tperson_bdmother]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_bdmother]  DEFAULT (N'1300/01/01') FOR [bdmother]
GO
/****** Object:  Default [DF_tperson_nmtavalod]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_nmtavalod]  DEFAULT (N'0') FOR [nmtavalod]
GO
/****** Object:  Default [DF_tperson_ostansokonat]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_ostansokonat]  DEFAULT (N'همدان') FOR [ostansokonat]
GO
/****** Object:  Default [DF_tperson_bakhshsokonat]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_bakhshsokonat]  DEFAULT (N'مرکزی') FOR [bakhshsokonat]
GO
/****** Object:  Default [DF_tperson_cityvilagesokonat]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_cityvilagesokonat]  DEFAULT (N'-') FOR [cityvilagesokonat]
GO
/****** Object:  Default [DF_tperson_codeposti]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_codeposti]  DEFAULT (N'0000000000') FOR [codeposti]
GO
/****** Object:  Default [DF_tperson_addr]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_addr]  DEFAULT ('-') FOR [addr]
GO
/****** Object:  Default [DF_tperson_tell]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_tell]  DEFAULT (N'00000000000') FOR [tell]
GO
/****** Object:  Default [DF_tperson_mob1]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_mob1]  DEFAULT (N'00000000000') FOR [mob1]
GO
/****** Object:  Default [DF_tperson_mob2]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_mob2]  DEFAULT (N'00000000000') FOR [mob2]
GO
/****** Object:  Default [DF_tperson_emal]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_emal]  DEFAULT ((0)) FOR [emal]
GO
/****** Object:  Default [DF_tperson_idusersabt]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_idusersabt]  DEFAULT ((-1)) FOR [idusersabt]
GO
/****** Object:  Default [DF_tperson_iduseremal]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson] ADD  CONSTRAINT [DF_tperson_iduseremal]  DEFAULT ((-1)) FOR [iduseremal]
GO
/****** Object:  Default [DF_tuser_sex]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tuser] ADD  CONSTRAINT [DF_tuser_sex]  DEFAULT ((1)) FOR [sex]
GO
/****** Object:  Default [DF_tuser_isadmin]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tuser] ADD  CONSTRAINT [DF_tuser_isadmin]  DEFAULT ((2)) FOR [isadmin]
GO
/****** Object:  ForeignKey [FK_tperson_tmahal]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson]  WITH CHECK ADD  CONSTRAINT [FK_tperson_tmahal] FOREIGN KEY([idmtavalod])
REFERENCES [dbo].[tmahal] ([idmahal])
GO
ALTER TABLE [dbo].[tperson] CHECK CONSTRAINT [FK_tperson_tmahal]
GO
/****** Object:  ForeignKey [FK_tperson_tmahal1]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson]  WITH CHECK ADD  CONSTRAINT [FK_tperson_tmahal1] FOREIGN KEY([idmsokonat])
REFERENCES [dbo].[tmahal] ([idmahal])
GO
ALTER TABLE [dbo].[tperson] CHECK CONSTRAINT [FK_tperson_tmahal1]
GO
/****** Object:  ForeignKey [FK_tperson_tuser]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson]  WITH CHECK ADD  CONSTRAINT [FK_tperson_tuser] FOREIGN KEY([idusersabt])
REFERENCES [dbo].[tuser] ([iduser])
GO
ALTER TABLE [dbo].[tperson] CHECK CONSTRAINT [FK_tperson_tuser]
GO
/****** Object:  ForeignKey [FK_tperson_tuser1]    Script Date: 06/22/2019 11:03:20 ******/
ALTER TABLE [dbo].[tperson]  WITH CHECK ADD  CONSTRAINT [FK_tperson_tuser1] FOREIGN KEY([iduseremal])
REFERENCES [dbo].[tuser] ([iduser])
GO
ALTER TABLE [dbo].[tperson] CHECK CONSTRAINT [FK_tperson_tuser1]
GO
