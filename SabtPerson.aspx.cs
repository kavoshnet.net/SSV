﻿using System;
using System.Data;
using System.Drawing;
using System.Web.UI.WebControls;
using BijanComponents;
using DevExpress.Web.ASPxGridView;
using dstpersonTableAdapters;
using dsvpersonTableAdapters;

public partial class SabtPerson : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Label1.Text = "";
        if ((Session["VOROD"] != null) && (Session["ISADMIN"] != null))
        {
            if ((Session["VOROD"].ToString() == "ok") && (Session["ISADMIN"].ToString() == "1"))
            {
                if (!IsPostBack)
                {
                    BindGrid();
                    MasterASPxdgperson.FocusedRowIndex = 0;
                    JQDatePicker1.Text = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now).Trim();
                    //JQDatePicker1.MaxDate = DateTime.Now;
                    ddlmtavalod.SelectedValue = Session["IDMAHAL"].ToString();
                }
            }
            else
            {
                Label1.Text = "شما اجازه استفاده از این بخش را ندارید";
                Response.Redirect("Default.aspx");

            }
        }
        else
        {

            Response.Redirect("login.aspx");
        }
        MasterASPxdgperson.DataColumns["mother"].Settings.AutoFilterCondition = AutoFilterCondition.Contains;
        MasterASPxdgperson.DataColumns["addr"].Settings.AutoFilterCondition = AutoFilterCondition.Contains;
        MasterASPxdgperson.DataColumns["father"].Settings.AutoFilterCondition = AutoFilterCondition.Contains;

    }

    private void BindGrid()
    {
        try
        {

            //var sda = new vpersonTableAdapter();
            //var ds = new dsvperson();
            int iduser = Convert.ToInt16(Session["IDUSER"]);
            string datesabtfrom = ShamsiDate.GetShamsiDate(DateTime.Now.AddDays(-365)).Trim();
            string datesabtto = ShamsiDate.GetShamsiDate(DateTime.Now).Trim();
            int codemahaluser = Convert.ToInt16(Session["CODEMAHAL"]);
            /*sda.FillByoutbox(ds.vperson, iduser, datesabtfrom, datesabtto);
            dgperson.DataSource = ds.vperson;
            dgperson.DataBind();*/

            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            Mastersqldperson.SelectParameters.Clear();
            Mastersqldperson.SelectParameters.Add("datesabtfrom", datesabtfrom);
            Mastersqldperson.SelectParameters.Add("datesabtto", datesabtto);
            Mastersqldperson.SelectParameters.Add("iduser", iduser.ToString());
            Mastersqldperson.DataBind();
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------


            /*DetailASPxdgperson.DataSource = ds.vperson;
             DetailASPxdgperson.DataBind();*/

        }
        catch (Exception ex)
        {
            Label1.Text = ex.Message;
            Label1.Text = "خطای ناشناخته رخ داده است.";
        }
    }

    protected void btnInsert_Click(object sender, EventArgs e)
    {
        try
        {
            var sda = new tpersonTableAdapter();
            var ds = new dstperson();
            DateTime mybdate, bf, bt;

            //چک تاریخ تولد
            mybdate = ShamsiDate.ShamsiToGregory(JQDatePicker1.Text.Trim());
            bf = DateTime.Now.AddDays(-365);
            bt = DateTime.Now;
            if (mybdate > bt || mybdate < bf)
            {
                Label1.Text = "در ثبت تاریخ تولد دقت کنید";
                return;
            }
            if (MasterASPxdgperson.FocusedRowIndex != -1 /*  && txtLname.Text.Trim()!=""*/)
            {
                bool Success =
                    sda.FillByidperson(ds.tperson,
                        int.Parse(
                            MasterASPxdgperson.GetRowValues(MasterASPxdgperson.FocusedRowIndex, "idperson").ToString())) >
                    0;
                if (Success)
                {
                    string nmtavalodstr = "";
                    if (rbnmtavalod1.Checked)
                        nmtavalodstr = "بیمارستان";
                    else if (rbnmtavalod2.Checked)
                        nmtavalodstr = "منزل";
                    ds.tperson.Rows[0][ds.tperson.sexColumn] = rbsex1.Checked;
                    ds.tperson.Rows[0][ds.tperson.fathernameColumn] = txtfather.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.mothernameColumn] = txtmother.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.jfatherColumn] = txtjfather.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.jmotherColumn] = txtjmother.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.fatherfnameColumn] = "-";
                    ds.tperson.Rows[0][ds.tperson.motherfnameColumn] = "-";
                    ds.tperson.Rows[0][ds.tperson.ncfatherColumn] = txtncfather.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.bdateColumn] = JQDatePicker1.Text;
                    ds.tperson.Rows[0][ds.tperson.idmtavalodColumn] = ddlmtavalod.SelectedValue;
                    ds.tperson.Rows[0][ds.tperson.nmtavalodColumn] = nmtavalodstr;
                    ds.tperson.Rows[0][ds.tperson.addrColumn] = txtaddr.Text.Trim();
                    ds.tperson.Rows[0][ds.tperson.idmsokonatColumn] = ddlmsokonat.SelectedValue;
                    ds.tperson.Rows[0][ds.tperson.tellColumn] = txttell.Text.Trim().PadLeft(11, '0');
                    ds.tperson.Rows[0][ds.tperson.mob1Column] = txtmob1.Text.Trim().PadLeft(11, '0');
                    ds.tperson.Rows[0][ds.tperson.mob2Column] = txtmob2.Text.Trim().PadLeft(11, '0');
                    ds.tperson.Rows[0][ds.tperson.dsabtColumn] =
                        BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now).Trim();
                    ds.tperson.Rows[0][ds.tperson.demalColumn] = "";
                    ds.tperson.Rows[0][ds.tperson.emalColumn] = false;
                    ds.tperson.Rows[0][ds.tperson.idusersabtColumn] = Convert.ToInt16(Session["IDUSER"]);
                    sda.Update(ds.tperson);
                    Label1.Text = "ویرایش داده ها با موفقیت در انجام شد.";
                }
            }
            else
            {
                DataRow MyRow = ds.tperson.NewRow();

                string nmtavalodstr = "";
                if (rbnmtavalod1.Checked)
                    nmtavalodstr = "بیمارستان";
                else if (rbnmtavalod2.Checked)
                    nmtavalodstr = "منزل";

                MyRow[ds.tperson.sexColumn] = rbsex1.Checked;
                MyRow[ds.tperson.fathernameColumn] = txtfather.Text.Trim();
                MyRow[ds.tperson.mothernameColumn] = txtmother.Text.Trim();
                MyRow[ds.tperson.jfatherColumn] = txtjfather.Text.Trim();
                MyRow[ds.tperson.jmotherColumn] = txtjmother.Text.Trim();
                MyRow[ds.tperson.fatherfnameColumn] = "-";
                MyRow[ds.tperson.motherfnameColumn] = "-";
                MyRow[ds.tperson.ncfatherColumn] = txtncfather.Text.Trim();
                MyRow[ds.tperson.bdateColumn] = JQDatePicker1.Text;
                MyRow[ds.tperson.idmtavalodColumn] = ddlmtavalod.SelectedValue;
                MyRow[ds.tperson.nmtavalodColumn] = nmtavalodstr;
                MyRow[ds.tperson.addrColumn] = txtaddr.Text.Trim();
                MyRow[ds.tperson.idmsokonatColumn] = ddlmsokonat.SelectedValue;
                MyRow[ds.tperson.tellColumn] = txttell.Text.Trim().PadLeft(11, '0');
                MyRow[ds.tperson.mob1Column] = txtmob1.Text.Trim().PadLeft(11, '0');
                MyRow[ds.tperson.mob2Column] = txtmob2.Text.Trim().PadLeft(11, '0');
                MyRow[ds.tperson.dsabtColumn] = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now).Trim();
                MyRow[ds.tperson.demalColumn] = "";
                MyRow[ds.tperson.emalColumn] = false;
                MyRow[ds.tperson.idusersabtColumn] = Convert.ToInt16(Session["IDUSER"]);

                ds.tperson.Rows.Add(MyRow);
                sda.Update(ds.tperson);

                Label1.Text = "اطلاعات  جدید با موفقیت در سیستم ذخیره گردید";
            }
            ClearTextBox();
            BindGrid();
            MasterASPxdgperson.FocusedRowIndex = 0;
        }
        catch (Exception ex)
        {
            Label111.Text = ex.Message;
            Label111.Text = "در ثبت داده ها خطایی رخ داده است لطفا دوباره تلاش کنید.";
        }
    }

    private void ClearTextBox()
    {
        txtfather.Text = "";
        txtmother.Text = "";
        txtjfather.Text = "";
        txtjmother.Text = "";
        txtncfather.Text = "";
        JQDatePicker1.Text = BijanComponents.ShamsiDate.GetShamsiDate(DateTime.Now).Trim();
        ddlmtavalod.SelectedIndex = -1;
        txtaddr.Text = "";
        ddlmsokonat.SelectedIndex = -1;
        //txttell.Text = "00000000000";
        //txtmob1.Text = "00000000000";
        //txtmob2.Text = "00000000000";
        txttell.Text = "";
        txtmob1.Text = "";
        txtmob2.Text = "";
        ddlmtavalod.SelectedValue = Session["IDMAHAL"].ToString();

    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            if (MasterASPxdgperson.FocusedRowIndex != -1)
                //if (txtmother.Text.Trim() != "" && txtmother.Text.Trim()!="" && dgperson.SelectedIndex > -1)
            {
                var sda = new tpersonTableAdapter();
                sda.Delete(
                    int.Parse(MasterASPxdgperson.GetRowValues(MasterASPxdgperson.FocusedRowIndex, "idperson").ToString()));
                Label1.Text = "حذف  با موفقیت انجام شد";
                BindGrid();
                ClearTextBox();
                MasterASPxdgperson.FocusedRowIndex = 0;
            }
        }
        catch (Exception ex)
        {
            Label1.Text = ex.Message;
            Label1.Text = "در حذف داده ها خطایی رخ داده است لطفا دوباره تلاش کنید.";
            return;
        }
    }

    protected void Cancel_Click(object sender, EventArgs e)
    {
        MasterASPxdgperson.FocusedRowIndex = 0;
        ClearTextBox();
        txtmother.Focus();
    }


    protected void MasterASPxdgperson_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
        {
            string temp = e.GetValue("bdate").ToString();
            DateTime tow = ShamsiDate.ShamsiToGregory(temp);
            DateTime from = DateTime.Now;
            int y = 15 - from.Subtract(tow).Days;
            if (y >= 1 && y <= 5)
            {
                e.Row.BackColor = Color.Khaki;
            }
            else if (y < 1)
            {
                e.Row.BackColor = Color.Tomato;
            }
            Label label = MasterASPxdgperson.FindRowCellTemplateControl(e.VisibleIndex, null, "Label1") as Label;

            //var x = (Label)e.Row.Cells[7].FindControl("Label3");
            if (y > 0)
                label.Text = y + " روز ";
            else
                label.Text = "خارج";
        }

    }

    protected void DetailASPxdgperson_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["idperson"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }


    protected void LinkButton1_Click(object sender, EventArgs e)
    {

        tpersonTableAdapter sda = new tpersonTableAdapter();
        dstperson ds = new dstperson();
        sda.FillByidperson(ds.tperson,
            int.Parse(MasterASPxdgperson.GetRowValues(MasterASPxdgperson.FocusedRowIndex, "idperson").ToString()));
        if (ds.tperson.Rows[0][ds.tperson.sexColumn].ToString().ToLower() == "true")
        {
            rbsex1.Checked = true;
            rbsex2.Checked = false;
        }
        else
        {rbsex2.Checked = true;
            rbsex1.Checked = false;
        }

        txtfather.Text = ds.tperson.Rows[0][ds.tperson.fathernameColumn].ToString();
        txtmother.Text = ds.tperson.Rows[0][ds.tperson.mothernameColumn].ToString();
        txtjfather.Text = ds.tperson.Rows[0][ds.tperson.jfatherColumn].ToString();
        txtjmother.Text = ds.tperson.Rows[0][ds.tperson.jmotherColumn].ToString();
        txtncfather.Text = ds.tperson.Rows[0][ds.tperson.ncfatherColumn].ToString();
        JQDatePicker1.Text = ds.tperson.Rows[0][ds.tperson.bdateColumn].ToString();
        ddlmtavalod.SelectedValue = ds.tperson.Rows[0][ds.tperson.idmtavalodColumn].ToString();
        if (ds.tperson.Rows[0][ds.tperson.nmtavalodColumn].ToString() == "بیمارستان")
        {
            rbnmtavalod1.Checked = true;
            rbnmtavalod2.Checked = false;
        }
        else
        {
            rbnmtavalod2.Checked = true;
            rbnmtavalod1.Checked = false;
        }

        txtaddr.Text = ds.tperson.Rows[0][ds.tperson.addrColumn].ToString();
        ddlmsokonat.SelectedValue = ds.tperson.Rows[0][ds.tperson.idmsokonatColumn].ToString();
        txttell.Text = ds.tperson.Rows[0][ds.tperson.tellColumn].ToString();
        txtmob1.Text = ds.tperson.Rows[0][ds.tperson.mob1Column].ToString();
        txtmob2.Text = ds.tperson.Rows[0][ds.tperson.mob2Column].ToString();
    }
    protected void btnnew_Click(object sender, EventArgs e)
    {
        MasterASPxdgperson.FocusedRowIndex = -1;
        ClearTextBox();
        txtmother.Focus();
    }
}