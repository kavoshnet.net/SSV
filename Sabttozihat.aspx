﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
         CodeFile="Sabttozihat.aspx.cs" Inherits="Sabttozihat" Title="ثبت توضیحات" %>


<%@ Register assembly="JQControls" namespace="JQControls" tagprefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"><p class="panel-info" style="color: darkgreen" >توضیحات شناسنامه نوزاد</p>
            </div>
            <div class="panel-body">
    
    <p>
        <asp:Label ID="Label6" runat="server" Font-Bold="True" ForeColor="black">همکاران محترم در نظر داشته باشند که نوع کاغذ را در هنگام چاپ به شناسنامه تغییر دهند</asp:Label>
    </p>
                
            <table class="tbl" align="right">
                <tr>
                    <td>
                        <asp:Label ID="Label25" runat="server" Text="توضیحات:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox1" runat="server" Height="197px" TextMode="MultiLine" 
                            Width="454px" Font-Names="Tahoma"></asp:TextBox>
                    </td>
                </tr>
        <tr>
                    <td>
                        &nbsp;
                    </td>
            <td align="center"> 
                <asp:Button ID="btnprint" runat="server" CssClass="btn btn-primary btn-xs" 
                            OnClick="btnprint_Click" Text="چاپ" CausesValidation="False"/>
            </td>
            </tr>
            </table>
            </div>
            </div>
            </div>
<div class="col-lg-12">
</div>                
            </div>
            </div>

</asp:Content>