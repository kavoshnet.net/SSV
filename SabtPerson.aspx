﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SabtPerson.aspx.cs" Inherits="SabtPerson" Title="ثبت ولادت" %>

<%@ Register assembly="JQControls" namespace="JQControls" tagprefix="cc1" %>

<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"><p class="panel-info" style="color: darkgreen" >لیست متولدین اداره ثبت احوال  <%= this.Session["NAMEMAHAL"].ToString()%></p>
            </div>
            <div class="panel-body">
    
  <fieldset>
   <legend><b> ثبت مشخصات ولادت </b> </legend>
    <table  align="center">
        <tr>
            <td align="center" colspan="2" style="height: 22px">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" style="width: 136px">
                <asp:Label ID="Label18" runat="server" Text="جنسیت:"></asp:Label>
            </td>
            <td align="right">
                <asp:RadioButton ID="rbsex1" runat="server" GroupName="Jensiat" Text="پسر" Checked="True" />
                <asp:RadioButton ID="rbsex2" runat="server" GroupName="Jensiat" Text="دختر" />
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 136px">
                <asp:Label ID="Label7" runat="server" Text="نام و نام خانوادگی مادر:"></asp:Label>
            </td>
            <td align="right">
                <asp:TextBox ID="txtmother" runat="server" CssClass="txt" Width="180px"></asp:TextBox>
                <asp:Label ID="Label24" runat="server" Text="*" ForeColor="Red"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" 
                    ControlToValidate="txtmother" 
                    ErrorMessage="لطفا نام و نام خانوادگی مادر را وارد کنید" Enabled="False"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 136px">
                <asp:Label ID="Label111" runat="server" Text="نام و نام خانوادگی پدر:"></asp:Label>
            </td>
            <td align="right">
                <asp:TextBox ID="txtfather" runat="server" CssClass="txt" Width="180px"></asp:TextBox>
                <asp:Label ID="Label22" runat="server" Text="*" ForeColor="Red"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                    ControlToValidate="txtfather" 
                    ErrorMessage="لطفا نام و نام خانوادگی پدر را وارد کنید" Enabled="False"></asp:RequiredFieldValidator>
                </td>
        </tr>
        <tr>
            <td align="right" style="width: 136px">
                <asp:Label ID="Label112" runat="server" Text="جدپدری:"></asp:Label>
            </td>
            <td align="right">
                <asp:TextBox ID="txtjfather" runat="server" CssClass="txt" Width="180px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 136px">
                <asp:Label ID="Label113" runat="server" Text="جدمادری:"></asp:Label>
            </td>
            <td align="right">
                <asp:TextBox ID="txtjmother" runat="server" CssClass="txt" Width="180px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 136px">
                <asp:Label ID="Label8" runat="server" Text="کدملی پدر:" Visible="False"></asp:Label>
            </td>
            <td align="right">
                <asp:TextBox ID="txtncfather" runat="server" CssClass="txt" Width="180px" 
                    Visible="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 136px">
                <asp:Label ID="Label9" runat="server" Text="تاریخ تولد:"></asp:Label>
            </td>
            <td align="right">
                <cc1:JQLoader ID="JQLoader1" runat="server" Theme="Blitzer" />
                <cc1:JQDatePicker ID="JQDatePicker1" runat="server" AnimationSpeed="Fast" 
                    AnimationType="Fade" CssClass="txtl" DateFormat="YMD" FirstDay="Saturday" 
                    IEDateFormat="YMD" IsRTL="True" Regional="fa"></cc1:JQDatePicker>
                <asp:Label ID="Label23" runat="server" Text="*" ForeColor="Red"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
                    ControlToValidate="JQDatePicker1" 
                    ErrorMessage="لطفا تاریخ تولد نوزاد را وارد کنید"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 136px">
                <asp:Label ID="Label11" runat="server" Text="محل تولد:"></asp:Label>
            </td>
            <td align="right">
                <asp:DropDownList ID="ddlmtavalod" runat="server" CssClass="ddl" DataSourceID="sqldmtavalod"
                    DataTextField="namemahal" DataValueField="idmahal" Width="180px">
                </asp:DropDownList>
                <asp:SqlDataSource ID="sqldmtavalod" runat="server" ConnectionString="<%$ ConnectionStrings:dbsabtveladatConnectionString %>"
                    
                    SelectCommand="SELECT idmahal, namemahal, codemahal FROM tmahal WHERE (idmahal &lt;&gt; 0)"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 136px">
                <asp:Label ID="Label19" runat="server" Text="نوع محل تولد:" Visible="False"></asp:Label>
            </td>
            <td align="right">
                <asp:RadioButton ID="rbnmtavalod1" runat="server" GroupName="mtavalod" Text="بیمارستان"
                    Checked="True" Visible="False" />
                <asp:RadioButton ID="rbnmtavalod2" runat="server" GroupName="mtavalod" 
                    Text="منزل" Visible="False" />
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 136px">
                <asp:Label ID="Label14" runat="server" Text="محل سکونت:" CssClass="lbl"></asp:Label>
                <br />
            </td>
            <td align="right" >
                <asp:DropDownList ID="ddlmsokonat" runat="server" CssClass="ddl" DataSourceID="sqldmsokonat"
                    DataTextField="namemahal" DataValueField="idmahal" Width="180px">
                </asp:DropDownList>
                <asp:SqlDataSource ID="sqldmsokonat" runat="server" ConnectionString="<%$ ConnectionStrings:dbsabtveladatConnectionString %>"
                    
                    SelectCommand="SELECT idmahal, namemahal, codemahal FROM tmahal WHERE (idmahal &lt;&gt; 0)"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 136px">
                <asp:Label ID="Label15" runat="server" Text="تلفن منزل:"></asp:Label>
            </td>
            <td align="right">
                <asp:TextBox ID="txttell" runat="server" CssClass="txt" Width="180px" 
                    MaxLength="11"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                    ControlToValidate="txttell" ErrorMessage="تنها میتوانید عدد وارد کنید" 
                    SetFocusOnError="True" ValidationExpression="\d+"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 136px">
                <asp:Label ID="Label16" runat="server" Text="موبایل1:"></asp:Label>
            </td>
            <td align="right">
                <asp:TextBox ID="txtmob1" runat="server" CssClass="txt" Width="180px" 
                    MaxLength="11"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                    ControlToValidate="txtmob1" ErrorMessage="تنها میتوانید عدد وارد کنید" 
                    SetFocusOnError="True" ValidationExpression="\d+"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 136px">
                <asp:Label ID="Label17" runat="server" Text="موبایل2:" Visible="False"></asp:Label>
            </td>
            <td align="right" valign="top">
                <asp:TextBox ID="txtmob2" runat="server" CssClass="txt" Width="180px" 
                    MaxLength="11" Visible="False"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                    ControlToValidate="txtmob2" ErrorMessage="تنها میتوانید عدد وارد کنید" 
                    SetFocusOnError="True" ValidationExpression="\d+" Enabled="False"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 136px">
                <asp:Label ID="Label20" runat="server" Text="آدرس:"></asp:Label>
            </td>
            <td align="right">
                <asp:TextBox ID="txtaddr" runat="server" CssClass="txt" Width="500px" Height="66px"
                    TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 136px"></td>
            <td align="right">
                    <asp:Button ID="btnnew" runat="server" CssClass="btn btn-info btn-xs" 
                    OnClick="btnnew_Click" Text="جدید" CausesValidation="False"/>
                <asp:Button ID="btninsert" runat="server" CssClass="btn btn-info btn-xs" 
                    OnClick="btnInsert_Click" Text="ذخیره" />
                <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-info btn-xs" OnClick="Cancel_Click" Text="انصراف"
                    CausesValidation="False" />
                    <asp:Button ID="btndelete" runat="server" CssClass="btn btn-info btn-xs" OnClick="btnDelete_Click" Text="حذف" CausesValidation="False" OnClientClick="return confirm('آیا برای حذف اطمینان دارید؟');"/>
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 136px">
            </td>
            <td align="center" valign="top">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="#CC0000"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="2" style="height: 15px">
            </td>
        </tr>
    </table>
    </fieldset>
            </div>
            </div>
            </div>
<div class="col-lg-12">
    <dx:ASPxGridView ID="MasterASPxdgperson" runat="server" 
        AutoGenerateColumns="False" DataSourceID="Mastersqldperson" 
        EnableTheming="True" KeyFieldName="idperson" 
        onhtmlrowprepared="MasterASPxdgperson_HtmlRowPrepared" RightToLeft="True" 
        Theme="Glass" Width="100%">
        <Columns>
            <dx:GridViewDataTextColumn Caption="مادر" FieldName="mother" VisibleIndex="0">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="پدر" FieldName="father" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="آدرس" FieldName="addr" Visible="False" 
                VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="تاریخ تولد" FieldName="bdate" 
                VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="تلفن" FieldName="tell" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="موبایل" FieldName="mob1" VisibleIndex="8">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="انجام" Visible="False" VisibleIndex="13">
                <DataItemTemplate>
                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Emal" 
                        onclick="LinkButton1_Click" 
                        onclientclick=" return confirm('آیا برای ثبت اطمینان دارید?') ">انجام</asp:LinkButton>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="idperson" Visible="False" 
                VisibleIndex="11">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="کاربر ثبت" FieldName="nameusersabt" 
                Visible="False" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="تاریخ ثبت" FieldName="dsabt" 
                Visible="False" VisibleIndex="9">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="مهلت" VisibleIndex="10">
                <DataItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="محل سکونت" FieldName="namemsokonat" 
                VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ویرایش" VisibleIndex="12">
                <DataItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" onclick="LinkButton1_Click">ویرایش</asp:LinkButton>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="جدپدری" FieldName="jfather" 
                VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="جدمادری" FieldName="jmother" 
                VisibleIndex="1">
            </dx:GridViewDataTextColumn>
        </Columns>
        <SettingsBehavior AllowFocusedRow="True" EnableRowHotTrack="True" />
        <SettingsPager>
            <Summary AllPagesText="صفحات : {0} - {1} ({2} مورد )" 
                Text="صفحه {0} از {1} ({2} مورد)" />
        </SettingsPager>
        <Settings ShowFilterRow="True" />
        <SettingsText EmptyDataRow="داده ای برای نمایش وجود ندارد" />
        <SettingsDetail ShowDetailRow="True" />
        <Styles>
            <AlternatingRow Enabled="True">
            </AlternatingRow>
        </Styles>
        <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="DetailASPxdgperson" runat="server" 
                    AutoGenerateColumns="False" DataSourceID="Detailsqldperson" 
                    EnableTheming="True" KeyFieldName="idperson" 
                    onbeforeperformdataselect="DetailASPxdgperson_BeforePerformDataSelect" 
                    RightToLeft="True" Theme="Office2003Blue" Width="100%">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="مادر" FieldName="mother" Visible="False" 
                            VisibleIndex="1">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="پدر" FieldName="father" Visible="False" 
                            VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="آدرس" FieldName="addr" VisibleIndex="3">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="ت.ت" FieldName="bdate" Visible="False" 
                            VisibleIndex="4">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="تلفن" FieldName="tell" Visible="False" 
                            VisibleIndex="7">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="موبایل" FieldName="mob1" Visible="False" 
                            VisibleIndex="8">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="انجام" Visible="False" VisibleIndex="0">
                            <DataItemTemplate>
                                <asp:LinkButton ID="LinkButton5" runat="server" CommandName="Emal" 
                                    onclick="LinkButton1_Click" 
                                    onclientclick=" return confirm('آیا برای ثبت اطمینان دارید?') ">انجام</asp:LinkButton>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="idperson" Visible="False" 
                            VisibleIndex="10">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="کاربر ثبت" FieldName="nameusersabt" 
                            VisibleIndex="8">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="تاریخ ثبت" FieldName="dsabt" 
                            VisibleIndex="11">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="مهلت" Visible="False" VisibleIndex="6">
                            <DataItemTemplate>
                                <asp:Label ID="Label58" runat="server" Text="Label"></asp:Label>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <SettingsBehavior AllowFocusedRow="True" EnableRowHotTrack="True" />
                    <SettingsBehavior AllowFocusedRow="True" EnableRowHotTrack="True" />
                    <SettingsBehavior AllowFocusedRow="True" EnableRowHotTrack="True" />
                    <SettingsBehavior AllowFocusedRow="True" EnableRowHotTrack="True" />
                    <Styles>
                        <AlternatingRow Enabled="True">
                        </AlternatingRow>
                    </Styles>
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>
    </dx:ASPxGridView>
                <asp:SqlDataSource ID="Mastersqldperson" runat="server" ConnectionString="<%$ ConnectionStrings:dbsabtveladatConnectionString %>"
                                   
                    
                    
        SelectCommand="SELECT addr, addruseremal, addrusersabt, bakhsh, bdate, bdfather, bdmother, bimarestan, cityvilage, codemahaluseremal, codemahalusersabt, codemsokonat, codemtavalod, codeposti, demal, dsabt, emal, father, fatherfname, fathername, fnameuseremal, fnameusersabt, houre, idmahaluseremal, idmahalusersabt, idmsokonat, idmtavalod, idperson, iduseremal, idusersabt, khanebehdasht, lnameuseremal, lnameusersabt, minute, mob1, mob2, mother, motherfname, mothername, name, namemahaluseremal, namemahalusersabt, namemsokonat, namemtavalod, nameuseremal, nameusersabt, ncfather, ncmother, nmtavalod, ostan, passworduseremal, passwordusersabt, sematuseremal, sematusersabt, seri, serial, sex, shahrestan, shomare, tell, telluseremal, tellusersabt, usernameuseremal, usernameusersabt, jfather, jmother FROM vperson WHERE (idusersabt = @iduser) AND (emal = 0) AND (dsabt &gt;= @datesabtfrom) AND (dsabt &lt;= @datesabtto)">
                    <SelectParameters>
                        <asp:Parameter Name="iduser" />
                        <asp:Parameter Name="datesabtfrom" />
                        <asp:Parameter Name="datesabtto" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="Detailsqldperson" runat="server" ConnectionString="<%$ ConnectionStrings:dbsabtveladatConnectionString %>"
                                   
                    
        SelectCommand="SELECT addr, addruseremal, addrusersabt, bakhsh, bdate, bdfather, bdmother, bimarestan, cityvilage, codemahaluseremal, codemahalusersabt, codemsokonat, codemtavalod, codeposti, demal, dsabt, emal, father, fatherfname, fathername, fnameuseremal, fnameusersabt, houre, idmahaluseremal, idmahalusersabt, idmsokonat, idmtavalod, idperson, iduseremal, idusersabt, khanebehdasht, lnameuseremal, lnameusersabt, minute, mob1, mob2, mother, motherfname, mothername, name, namemahaluseremal, namemahalusersabt, namemsokonat, namemtavalod, nameuseremal, nameusersabt, ncfather, ncmother, nmtavalod, ostan, passworduseremal, passwordusersabt, sematuseremal, sematusersabt, seri, serial, sex, shahrestan, shomare, tell, telluseremal, tellusersabt, usernameuseremal, usernameusersabt, jfather, jmother FROM vperson WHERE (idperson = @idperson)">
                    <SelectParameters>
                        <asp:SessionParameter Name="idperson" SessionField="idperson" />
                    </SelectParameters>
                </asp:SqlDataSource>
</div>                
            </div>
            </div>


</asp:Content>