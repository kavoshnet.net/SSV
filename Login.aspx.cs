﻿using System;
using System.Web.UI;
using dsvuserTableAdapters;

public partial class Login : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session.Clear();
        txtUserName.Focus();
        ////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        //btnLogin_Click(sender, e);
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            var ds = new dsvuser();
            var sda = new vuserTableAdapter();
            /////////////////////////////////
            /////////////////////////////////
            /////////////////////////////////
            /////////////////////////////////
            /////////////////////////////////
            //txtUserName.Text = "mkb4";
            //txtPass.Text = "mkb4";
            /////////////////////////////////
            /////////////////////////////////
            /////////////////////////////////
            /////////////////////////////////
            /////////////////////////////////
            /////////////////////////////////
            /////////////////////////////////
            sda.FillByuserpass(ds.vuser, txtUserName.Text.Trim(), txtPass.Text.Trim());
            if (ds.vuser.Rows.Count > 0)
            {
                Session["IDUSER"] = ds.vuser.Rows[0][ds.vuser.iduserColumn].ToString();
                Session["IDMAHAL"] = ds.vuser.Rows[0][ds.vuser.idmahalColumn].ToString();
                Session["FNAME"] = ds.vuser.Rows[0][ds.vuser.fnameColumn].ToString();
                Session["LNAME"] = ds.vuser.Rows[0][ds.vuser.lnameColumn].ToString();
                Session["SEX"] = ds.vuser.Rows[0][ds.vuser.sexColumn].ToString();
                Session["SEMAT"] = ds.vuser.Rows[0][ds.vuser.sematColumn].ToString();
                Session["MKHEDMAT"] = ds.vuser.Rows[0][ds.vuser.mkhedmatColumn].ToString();
                Session["ADDR"] = ds.vuser.Rows[0][ds.vuser.addrColumn].ToString();
                Session["NAMEMAHAL"] = ds.vuser.Rows[0][ds.vuser.namemahalColumn].ToString();
                Session["CODEMAHAL"] = ds.vuser.Rows[0][ds.vuser.codemahalColumn].ToString();
                Session["ISADMIN"] = ds.vuser.Rows[0][ds.vuser.isadminColumn].ToString();
                Session["VOROD"] = "ok";
                Response.Redirect("Default.aspx");
            }
            else
            {
                Label3.Text = "نام کاربری یا کلمه عبور اشتباه می باشد";
            }
        }
        catch (Exception ex)
        {
            Label3.Text = ex.Message;
            Label3.Text = "در ورود کاربر خطایی رخ داده است لطفا دوباره تلاش کنید";
        }
    }
}