﻿using System;
using System.Web.Configuration;
using System.Web.UI;
using dsvpersonTableAdapters;
using FastReport;
using FastReport.Data;
using BijanComponents;
public partial class printgovahi : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if ((Session["VOROD"] != null) && (Session["ISADMIN"] != null))
        {
            if ((Session["VOROD"].ToString() != "ok") || (Session["ISADMIN"].ToString() != "2"))
            {
                Response.Redirect("Default.aspx");
            }
        }
        else
        {
            Response.Redirect("login.aspx");
        }
        

        string connectionstr = WebConfigurationManager.ConnectionStrings["dbsabtveladatConnectionString"].ToString();
        /*string connectionstr =
            string.Format(
                "Data Source=.\\SQLEXPRESS;AttachDbFilename={0};Integrated Security=True;Connect Timeout=30;User Instance=True",
                Server.MapPath("~/APP_DATA/dbsabtveladat.mdf"));*/
        var rpt = new Report();
        //conection string with parameter------------------------------------------------------
        rpt.Load(Server.MapPath("~/APP_DATA/govahiasli.frx"));
        rpt.SetParameterValue("ConnectionStr", connectionstr);
        rpt.SetParameterValue("ConnectionStr", connectionstr);
        if (rpt.Dictionary.Connections.Count > 0)
        {
            rpt.Dictionary.Connections[0].ConnectionString = connectionstr;
            rpt.Dictionary.Connections[0].ConnectionStringExpression = "[ConnectionStr]";
            rpt.Dictionary.Connections[0].CommandTimeout = 60;
        }
        //load data with query-------------------------------------------------------------------
        var data = rpt.GetDataSource("vperson") as TableDataSource;
        if (Session["GOVAHIID"] != null)
        {
            data.SelectCommand = string.Format("SELECT * from vperson where idperson={0}",
                Session["GOVAHIID"]);
        }
        //get data -------------------------------------------------------------------------------
        var sda = new vpersonTableAdapter();
        var ds = new dsvperson();
        int idperson = Convert.ToInt16(Session["GOVAHIID"]);
        sda.FillByidperson(ds.vperson, idperson);

        //convert sex to persian format with parameter---------------------------------------------
        string sex = (ds.vperson.Rows[0][ds.vperson.sexColumn].ToString() == "True" ? "پسر" : "دختر");
        rpt.SetParameterValue("sex", sex);

        //convert date to persian format with parameter-----------------------------------------------
        string bdate = ds.vperson.Rows[0][ds.vperson.bdateColumn].ToString();

        //string shomare = ds.vperson.Rows[0][ds.vperson.shomareColumn].ToString();
        var temp = new my_inttopersian();
        /*bdate = temp.num2str(Convert.ToInt16(bdate.Substring(8, 2)).ToString()) +
                temp.my_persianmont(Convert.ToInt16(bdate.Substring(5, 2))) + " " +
                temp.num2str(bdate.Substring(0, 4));*/
        string day, month, year;
        string dday, dmonth, dyear;
        day = temp.num2str(Convert.ToInt16(bdate.Substring(8, 2)).ToString());
        month = temp.my_persianmont(Convert.ToInt16(bdate.Substring(5, 2)));
        year = temp.num2str(bdate.Substring(0, 4));
        day = temp.num2str(Convert.ToInt16(bdate.Substring(8, 2)).ToString());
        month = temp.my_persianmont(Convert.ToInt16(bdate.Substring(5, 2)));
        year = temp.num2str(bdate.Substring(0, 4));
        dday = bdate.Substring(8, 2);
        dmonth = bdate.Substring(5, 2);
        dyear = bdate.Substring(2, 2);

        string DateNow = ShamsiDate.GetShamsiDate(DateTime.Now).Trim();
        rpt.SetParameterValue("bdate", DateNow);
        
        rpt.SetParameterValue("day", day);
        rpt.SetParameterValue("month", month);
        rpt.SetParameterValue("year", year);

        rpt.SetParameterValue("dday", dday);
        rpt.SetParameterValue("dmonth", dmonth);
        rpt.SetParameterValue("dyear", dyear);

        //rpt.SetParameterValue("bdate", bdate);

        //convert semat to checkbox with parameter---------------------------------------------------
        string semat = ds.vperson.Rows[0][ds.vperson.sematusersabtColumn].ToString();
        var semat1 = rpt.FindObject("CheckBox1") as CheckBoxObject;
        var semat2 = rpt.FindObject("CheckBox2") as CheckBoxObject;
        var semat3 = rpt.FindObject("CheckBox3") as CheckBoxObject;
        if (semat == "1")
        {
            semat1.Visible = true;
            semat2.Visible = false;
            semat3.Visible = false;
        }
        else if (semat == "2")
        {
            semat1.Visible = false;
            semat2.Visible = true;
            semat3.Visible = false;
        }
        else if (semat == "3")
        {
            semat1.Visible = false;
            semat2.Visible = false;
            semat3.Visible = true;
        }

        //convert nmtavalod to checkbox with parameter---------------------------------------------------
        string nmtavalod = ds.vperson.Rows[0][ds.vperson.nmtavalodColumn].ToString();
        var nmtavalod1 = rpt.FindObject("CheckBox4") as CheckBoxObject;
        var nmtavalod2 = rpt.FindObject("CheckBox5") as CheckBoxObject;
        if (nmtavalod == "منزل")
        {
            nmtavalod1.Visible = true;
            nmtavalod2.Visible = false;
        }
        else if (nmtavalod == "سایر")
        {
            nmtavalod1.Visible = false;
            nmtavalod2.Visible = true;
        }
        else if (nmtavalod == "هیچکدام")
        {
            nmtavalod1.Visible = false;
            nmtavalod2.Visible = false;
        }
        else
        {
            nmtavalod1.Visible = false;
            nmtavalod2.Visible = false;
        }
        //------------------------------------------------------------------------------------------
        rpt.Prepare();
        WebReport1.Report = rpt;
        WebReport1.PdfEmbeddingFonts = true;
        WebReport1.LocalizationFile = Server.MapPath("~/APP_DATA/Persian.frl");
        //WebReport1.Report.LoadPrepared(this.Server.MapPath("~/App_Data/govahiasli.fpx"));
        WebReport1.ReportDone = true;
        //rpt.ShowPrepared();
    }
}