﻿using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using BijanComponents;
using DevExpress.Web.ASPxGridView;
using dsvpersonTableAdapters;

public partial class InBoxListPerson : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        Label1.Text = "";
        if ((Session["VOROD"] != null) && (Session["ISADMIN"] != null))
        {
            if ((Session["VOROD"].ToString() == "ok") && (Session["ISADMIN"].ToString() == "1"))
            {
                if (!IsPostBack)
                {
                    BindGrid();
                }
            }
            else
            {
                Label1.Text = "شما اجازه استفاده از این بخش را ندارید";
                Response.Redirect("Default.aspx");

            }
        }
        else
        {
            Response.Redirect("login.aspx");
        }
        MasterASPxdgperson.DataColumns["mother"].Settings.AutoFilterCondition = AutoFilterCondition.Contains;
        MasterASPxdgperson.DataColumns["addr"].Settings.AutoFilterCondition = AutoFilterCondition.Contains;
    }

    private void BindGrid()
    {
        try
        {
            
            //var sda = new vpersonTableAdapter();
            //var ds = new dsvperson();
            int iduser = Convert.ToInt16(Session["IDUSER"]);
            string datesabtfrom = ShamsiDate.GetShamsiDate(DateTime.Now.AddDays(-365)).Trim();
            string datesabtto = ShamsiDate.GetShamsiDate(DateTime.Now).Trim();
            int codemahaluser = Convert.ToInt16(Session["CODEMAHAL"]);
            //sda.FillByinbox(ds.vperson, iduser, datesabtfrom, datesabtto, codemahaluser);
            //MasterASPxdgperson.DataSource = ds.vperson;
            //MasterASPxdgperson.DataBind();
            
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            Mastersqldperson.SelectParameters.Clear();
            Mastersqldperson.SelectParameters.Add("datesabtfrom", datesabtfrom);
            Mastersqldperson.SelectParameters.Add("datesabtto", datesabtto);
            Mastersqldperson.SelectParameters.Add("codemahaluser", codemahaluser.ToString());
            Mastersqldperson.SelectParameters.Add("idusersabt", iduser.ToString());
            Mastersqldperson.DataBind();
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------
            //-------------------------------------------


            /*            DetailASPxdgperson.DataSource = ds.vperson;
                                                                                                            DetailASPxdgperson.DataBind();*/

        }
        catch (Exception ex)
        {
            Label1.Text = ex.Message;
            Label1.Text = "خطای ناشناخته رخ داده است.";
        }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        try
        {
                var sda = new vpersonTableAdapter();
                var ds = new dsvperson();
                int selectedvalue = int.Parse(MasterASPxdgperson.GetRowValues(MasterASPxdgperson.FocusedRowIndex, "idperson").ToString());
                if (selectedvalue != 0)
                {
                    bool Success = sda.FillByidperson(ds.vperson, selectedvalue) > 0;
                    if (Success)
                    {
                        sda.UpdateSetEmal(true, ShamsiDate.GetShamsiDate(DateTime.Now).Trim(),
                            Convert.ToInt16(Session["IDUSER"]), selectedvalue);
                        Response.Redirect("InBoxListPerson.aspx");
                    }
                }
            }
        catch (Exception ex)
        {
            Label1.Text = ex.Message;
            Label1.Text = "در اصلاح داده ها خطایی رخ داده است لطفا دوباره تلاش کنید.";
        }


    }
    protected void MasterASPxdgperson_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
        {
            string temp = e.GetValue("bdate").ToString();
            DateTime tow = ShamsiDate.ShamsiToGregory(temp);
            DateTime from = DateTime.Now;
            int y = 15 - from.Subtract(tow).Days;
            if (y >= 1 && y <= 5)
            {
                e.Row.BackColor = Color.Khaki;
            }
            else if (y < 1)
            {
                e.Row.BackColor = Color.Tomato;
            }
            Label label = MasterASPxdgperson.FindRowCellTemplateControl(e.VisibleIndex, null, "Label1") as Label;

            //var x = (Label)e.Row.Cells[7].FindControl("Label3");
            if (y > 0)
                label.Text = y + " روز ";
            else
                label.Text = "خارج";
        }

    }
    protected void DetailASPxdgperson_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["idperson"] = (sender as ASPxGridView).GetMasterRowKeyValue();}
}