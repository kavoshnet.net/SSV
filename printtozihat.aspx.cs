﻿using System;
using System.Web.Configuration;
using System.Web.UI;
using dsvpersonTableAdapters;
using FastReport;
using FastReport.Data;
using BijanComponents;
public partial class printtozihat : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if ((Session["VOROD"] != null) && (Session["ISADMIN"] != null))
        {
            if ((Session["VOROD"].ToString() != "ok") || (Session["ISADMIN"].ToString() != "1"))
            {
                Response.Redirect("Default.aspx");
            }
        }
        else
        {
            Response.Redirect("login.aspx");
        }

        var rpt = new Report();
        //conection string with parameter------------------------------------------------------
        rpt.Load(Server.MapPath("~/APP_DATA/tozihatveladat.frx"));
        rpt.SetParameterValue("tozihat", Session["tozihat"].ToString());
        //------------------------------------------------------------------------------------------
        rpt.Prepare();
        WebReport1.Report = rpt;
        WebReport1.LocalizationFile = Server.MapPath("~/APP_DATA/Persian.frl");
        //WebReport1.Report.LoadPrepared(this.Server.MapPath("~/App_Data/govahiasli.fpx"));
        WebReport1.ReportDone = true;
        //rpt.ShowPrepared();


    }
}