﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
         CodeFile="InBoxListPerson.aspx.cs" Inherits="InBoxListPerson" Title="لیست ولادتهای ارسالی" %>


<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"><p class="panel-info" style="color: darkgreen" >لیست متولدین اداره ثبت احوال  <%= this.Session["NAMEMAHAL"].ToString()%></p>
            </div>
            <div class="panel-body">
                <dx:ASPxGridView ID="MasterASPxdgperson" runat="server" AutoGenerateColumns="False" 
                    EnableTheming="True" KeyFieldName="idperson" RightToLeft="True" Theme="Glass" 
                    Width="100%"  onhtmlrowprepared="MasterASPxdgperson_HtmlRowPrepared" 
                    DataSourceID="Mastersqldperson">

                    <Columns>
                        <dx:GridViewDataTextColumn Caption="مادر" FieldName="mother" VisibleIndex="0">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="ش.م مادر" FieldName="ncmother" VisibleIndex="0">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="ش.م پدر" FieldName="ncfather" VisibleIndex="0">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="پدر" FieldName="father" VisibleIndex="1" 
                            Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="آدرس" FieldName="addr" VisibleIndex="2">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="ت.ت" FieldName="bdate" VisibleIndex="3" 
                            Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="تلفن" FieldName="tell" VisibleIndex="4">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="موبایل" FieldName="mob1" VisibleIndex="5">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn VisibleIndex="10" Caption="انجام">
                            <DataItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" 
                                    onclientclick=" return confirm('آیا برای ثبت اطمینان دارید?') " 
                                    CommandName="Emal" onclick="LinkButton1_Click">انجام</asp:LinkButton>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="idperson" Visible="False" 
                            VisibleIndex="7">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="کاربر ثبت" FieldName="nameusersabt" 
                            VisibleIndex="6" Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="تاریخ ثبت" FieldName="dsabt" 
                            VisibleIndex="7" Visible="False">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="مهلت" VisibleIndex="9">
                            <DataItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <SettingsBehavior AllowFocusedRow="True" EnableRowHotTrack="True" />

                    <SettingsPager>
                        <Summary AllPagesText="صفحات : {0} - {1} ({2} مورد )" 
                            Text="صفحه {0} از {1} ({2} مورد)" />
                    </SettingsPager>

                    <Settings ShowFilterRow="True" />
                    <SettingsText EmptyDataRow="داده ای برای نمایش وجود ندارد" />
                    <SettingsDetail ShowDetailRow="True" />

                    <Styles>
                        <AlternatingRow Enabled="True">
                        </AlternatingRow>
                    </Styles>
                    <Templates>
                        <DetailRow>
                            <dx:ASPxGridView ID="DetailASPxdgperson" runat="server" 
                                AutoGenerateColumns="False" EnableTheming="True" KeyFieldName="idperson" RightToLeft="True" 
                                Theme="Office2003Blue" Width="100%" DataSourceID="Detailsqldperson" 
                                onbeforeperformdataselect="DetailASPxdgperson_BeforePerformDataSelect">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="مادر" FieldName="mother" VisibleIndex="1" 
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="پدر" FieldName="father" VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="آدرس" FieldName="addr" VisibleIndex="5" 
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="تاریخ تولد" FieldName="bdate" 
                                        VisibleIndex="11">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="تلفن" FieldName="tell" VisibleIndex="6" 
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="موبایل" FieldName="mob1" VisibleIndex="7" 
                                        Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="انجام" VisibleIndex="0" Visible="False">
                                        <DataItemTemplate>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Emal" 
                                                onclick="LinkButton1_Click" 
                                                onclientclick=" return confirm('آیا برای ثبت اطمینان دارید?') ">انجام</asp:LinkButton>
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="idperson" Visible="False" 
                                        VisibleIndex="8">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="کاربر ثبت" FieldName="nameusersabt" 
                                        VisibleIndex="7">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="تاریخ ثبت" FieldName="dsabt" 
                                        VisibleIndex="13">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="مهلت" VisibleIndex="9" Visible="False">
                                        <DataItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="جدپدری" FieldName="jfather" 
                                        VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="جدمادری" FieldName="jmother" 
                                        VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" EnableRowHotTrack="True" />
                                <Styles>
                                    <AlternatingRow Enabled="True">
                                    </AlternatingRow>
                                </Styles>
                            </dx:ASPxGridView>
                        </DetailRow>
                    </Templates>
                </dx:ASPxGridView>
                <br />
                <asp:Label ID="Label1" runat="server" ForeColor="#CC0000" Text="Label"></asp:Label>
                <br />
                <asp:SqlDataSource ID="Mastersqldperson" runat="server" ConnectionString="<%$ ConnectionStrings:dbsabtveladatConnectionString %>"
                                   
                    
                    SelectCommand="SELECT addr, addruseremal, addrusersabt, bakhsh, bdate, bdfather, bdmother, bimarestan, cityvilage, codemahaluseremal, codemahalusersabt, codemsokonat, codemtavalod, codeposti, demal, dsabt, emal, father, fatherfname, fathername, fnameuseremal, fnameusersabt, houre, idmahaluseremal, idmahalusersabt, idmsokonat, idmtavalod, idperson, iduseremal, idusersabt, khanebehdasht, lnameuseremal, lnameusersabt, minute, mob1, mob2, mother, motherfname, mothername, name, namemahaluseremal, namemahalusersabt, namemsokonat, namemtavalod, nameusersabt, ncfather, ncmother, nmtavalod, ostan, passworduseremal, passwordusersabt, sematuseremal, sematusersabt, seri, serial, sex, shahrestan, shomare, tell, telluseremal, tellusersabt, usernameuseremal, usernameusersabt, jfather, jmother FROM vperson WHERE (emal = 0) AND (dsabt &gt;= @datesabtfrom) AND (dsabt &lt;= @datesabtto) AND (idusersabt &lt;&gt; @idusersabt) AND (codemsokonat = @codemahaluser)">
                    <SelectParameters>
                        <asp:Parameter Name="datesabtfrom" />
                        <asp:Parameter Name="datesabtto" />
                        <asp:Parameter Name="idusersabt" />
                        <asp:Parameter Name="codemahaluser" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="Detailsqldperson" runat="server" ConnectionString="<%$ ConnectionStrings:dbsabtveladatConnectionString %>"
                                   
                    
                    SelectCommand="SELECT addr, addruseremal, addrusersabt, bakhsh, bdate, bdfather, bdmother, bimarestan, cityvilage, codemahaluseremal, codemahalusersabt, codemsokonat, codemtavalod, codeposti, demal, dsabt, emal, father, fatherfname, fathername, fnameuseremal, fnameusersabt, houre, idmahaluseremal, idmahalusersabt, idmsokonat, idmtavalod, idperson, iduseremal, idusersabt, khanebehdasht, lnameuseremal, lnameusersabt, minute, mob1, mob2, mother, motherfname, mothername, name, namemahaluseremal, namemahalusersabt, namemsokonat, namemtavalod, nameusersabt, ncfather, ncmother, nmtavalod, ostan, passworduseremal, passwordusersabt, sematuseremal, sematusersabt, seri, serial, sex, shahrestan, shomare, tell, telluseremal, tellusersabt, usernameuseremal, usernameusersabt, jfather, jmother FROM vperson WHERE (idperson = @idperson)">
                    <SelectParameters>
                        <asp:SessionParameter Name="idperson" SessionField="idperson" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
            </div>
            </div>
<div class="col-lg-12">
</div>                
            </div>
            </div>

</asp:Content>